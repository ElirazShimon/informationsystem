-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2017 at 08:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webnicoi_fprat`
--

-- --------------------------------------------------------

--
-- Table structure for table `armed`
--

CREATE TABLE IF NOT EXISTS `armed` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `armed`
--

INSERT INTO `armed` (`id`, `name`) VALUES
(0, 'ללא נשק\r\n'),
(1, 'אקדח רשותי\r\n'),
(2, 'אקדח אישי\r\n'),
(3, 'נשק ארוך ואקדח רשותי\r\n'),
(4, 'נשק ארוך רשותי\r\n'),
(5, 'נשק ארוך אישי\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '300972973', 1478550155),
('employee', '112299002', 1481798851),
('employee', '305421968', 1478552346),
('employee', '56222323', 1481798907),
('teamleader', '222995244', 1478551953),
('teamleader', '252741963', 1481798933),
('teamleader', '300972974', 1478543982);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('createBuroc', 2, 'Admin can create new buroc', NULL, NULL, 1477479905, 1477479905),
('createEmployee', 2, 'Admin can create new employee', NULL, NULL, 1477479905, 1477479905),
('createInvitations', 2, 'Admin can create new invitation', NULL, NULL, 1477479905, 1477479905),
('createProject', 2, 'Team leader can create project', NULL, NULL, 1477479867, 1477479867),
('createRevenues', 2, 'Admin can create new revenue', NULL, NULL, 1477479906, 1477479906),
('createSubscribers', 2, 'Admin can create new subscriber', NULL, NULL, 1477479906, 1477479906),
('createSummary', 2, 'Team leader can create new summary', NULL, NULL, 1477479867, 1477479867),
('createSuppliers', 2, 'Admin can create new supplier', NULL, NULL, 1477479905, 1477479905),
('createUser', 2, 'Admin can create new user', NULL, NULL, 1477479904, 1477479904),
('createVisitors', 2, 'Admin can create new visitor', NULL, NULL, 1477479906, 1477479906),
('deleteBuroc', 2, 'Admin can delete  buroc', NULL, NULL, 1477479905, 1477479905),
('deleteEmployee', 2, 'Admin can delete  employee', NULL, NULL, 1477479905, 1477479905),
('deleteInvitations', 2, 'Admin can delete  invitation', NULL, NULL, 1477479906, 1477479906),
('deleteRevenues', 2, 'Admin can delete  revenue', NULL, NULL, 1477479906, 1477479906),
('deleteSubscribers', 2, 'Admin can delete  subscriber', NULL, NULL, 1477479906, 1477479906),
('deleteSummary', 2, 'Admin can delete  summary', NULL, NULL, 1477479905, 1477479905),
('deleteSuppliers', 2, 'Admin can delete  supplier', NULL, NULL, 1477479905, 1477479905),
('deleteUser', 2, 'Admin can delete users', NULL, NULL, 1477479905, 1477479905),
('deleteVisitors', 2, 'Admin can delete  visitor', NULL, NULL, 1477479906, 1477479906),
('employee', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('indexBuroc', 2, 'Admin can view all burocs', NULL, NULL, 1477479905, 1477479905),
('indexEmployee', 2, 'Index employee', NULL, NULL, 1477479815, 1477479815),
('indexInvitations', 2, 'Admin can view all invitations', NULL, NULL, 1477479905, 1477479905),
('indexProject', 2, 'Index Project', NULL, NULL, 1477479815, 1477479815),
('indexRevenues', 2, 'Admin can view all revenues', NULL, NULL, 1477479906, 1477479906),
('indexSubscribers', 2, 'Admin can view all subscribers', NULL, NULL, 1477479906, 1477479906),
('indexSummary', 2, 'Index summary', NULL, NULL, 1477479815, 1477479815),
('indexSuppliers', 2, 'Admin can view all suppliers', NULL, NULL, 1477479905, 1477479905),
('indexUser', 2, 'Admin can view all users', NULL, NULL, 1477479905, 1477479905),
('indexVisitors', 2, 'Admin can view all visitors', NULL, NULL, 1477479906, 1477479906),
('teamleader', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('updateBuroc', 2, 'Admin can update  buroc', NULL, NULL, 1477479905, 1477479905),
('updateEmployee', 2, 'Admin can update  employee', NULL, NULL, 1477479905, 1477479905),
('updateInvitations', 2, 'Admin can update  invitation', NULL, NULL, 1477479905, 1477479905),
('updateOwnPassword', 2, 'Every user can update his/her own password', NULL, NULL, 1477479815, 1477479815),
('updateOwnProject', 2, 'Team leader can update his only project', NULL, NULL, 1477479867, 1477479867),
('updateOwnSummary', 2, 'Team leader can update his only summary', NULL, NULL, 1477479867, 1477479867),
('updateOwnUser', 2, 'Every user can update his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('updatePassword', 2, 'Admin can update password for all users', NULL, NULL, 1477479905, 1477479905),
('updateProject', 2, 'Team leader can update project', NULL, NULL, 1477479867, 1477479867),
('updateRevenues', 2, 'Admin can update  revenue', NULL, NULL, 1477479906, 1477479906),
('updateSubscribers', 2, 'Admin can update  subscriber', NULL, NULL, 1477479906, 1477479906),
('updateSummary', 2, 'Team leader can update summary', NULL, NULL, 1477479867, 1477479867),
('updateSuppliers', 2, 'Admin can update  supplier', NULL, NULL, 1477479905, 1477479905),
('updateUser', 2, 'Admin can update all users', NULL, NULL, 1477479905, 1477479905),
('updateVisitors', 2, 'Admin can update  visitor', NULL, NULL, 1477479906, 1477479906),
('viewBuroc', 2, 'Admin can view each buroc', NULL, NULL, 1477479905, 1477479905),
('viewEmployee', 2, 'Admin can view each  employee', NULL, NULL, 1477479905, 1477479905),
('viewInvitations', 2, 'Admin can view each invitation', NULL, NULL, 1477479905, 1477479905),
('viewOwnEmployee', 2, 'Every employee can view his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('viewOwnUser', 2, 'Every user can view his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('viewProject', 2, 'View Project', NULL, NULL, 1477479815, 1477479815),
('viewRevenues', 2, 'Admin can view each revenue', NULL, NULL, 1477479906, 1477479906),
('viewSubscribers', 2, 'Admin can view each subscriber', NULL, NULL, 1477479906, 1477479906),
('viewSummary', 2, 'View summary', NULL, NULL, 1477479815, 1477479815),
('viewSuppliers', 2, 'Admin can view each supplier', NULL, NULL, 1477479905, 1477479905),
('viewUser', 2, 'Admin can view each user', NULL, NULL, 1477479905, 1477479905),
('viewVisitors', 2, 'Admin can view each visitor', NULL, NULL, 1477479906, 1477479906);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'createBuroc'),
('admin', 'createEmployee'),
('admin', 'createInvitations'),
('teamleader', 'createProject'),
('admin', 'createRevenues'),
('admin', 'createSubscribers'),
('teamleader', 'createSummary'),
('admin', 'createSuppliers'),
('admin', 'createUser'),
('admin', 'createVisitors'),
('admin', 'deleteBuroc'),
('admin', 'deleteEmployee'),
('admin', 'deleteInvitations'),
('admin', 'deleteRevenues'),
('admin', 'deleteSubscribers'),
('admin', 'deleteSummary'),
('admin', 'deleteSuppliers'),
('admin', 'deleteUser'),
('admin', 'deleteVisitors'),
('teamleader', 'employee'),
('admin', 'indexBuroc'),
('employee', 'indexEmployee'),
('admin', 'indexInvitations'),
('employee', 'indexProject'),
('admin', 'indexRevenues'),
('admin', 'indexSubscribers'),
('employee', 'indexSummary'),
('admin', 'indexSuppliers'),
('admin', 'indexUser'),
('admin', 'indexVisitors'),
('admin', 'teamleader'),
('admin', 'updateBuroc'),
('admin', 'updateEmployee'),
('admin', 'updateInvitations'),
('employee', 'updateOwnPassword'),
('teamleader', 'updateOwnProject'),
('teamleader', 'updateOwnSummary'),
('employee', 'updateOwnUser'),
('admin', 'updatePassword'),
('teamleader', 'updateProject'),
('admin', 'updateRevenues'),
('admin', 'updateSubscribers'),
('teamleader', 'updateSummary'),
('admin', 'updateSuppliers'),
('admin', 'updateUser'),
('admin', 'updateVisitors'),
('admin', 'viewBuroc'),
('admin', 'viewEmployee'),
('admin', 'viewInvitations'),
('employee', 'viewOwnEmployee'),
('employee', 'viewOwnUser'),
('employee', 'viewProject'),
('admin', 'viewRevenues'),
('admin', 'viewSubscribers'),
('employee', 'viewSummary'),
('admin', 'viewSuppliers'),
('admin', 'viewUser'),
('admin', 'viewVisitors');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bstatus`
--

CREATE TABLE IF NOT EXISTS `bstatus` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bstatus`
--

INSERT INTO `bstatus` (`id`, `name`) VALUES
(0, 'ממתין להתחלת טיפול'),
(1, 'בטיפול'),
(2, 'ממתין לאישור'),
(3, 'בהשהיה'),
(4, 'טופל');

-- --------------------------------------------------------

--
-- Table structure for table `buroc`
--

CREATE TABLE IF NOT EXISTS `buroc` (
  `subject` varchar(100) NOT NULL,
  `treatment` varchar(255) NOT NULL,
  `bstatus` int(100) NOT NULL,
  `DueDate` varchar(200) NOT NULL,
  `creatDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buroc`
--

INSERT INTO `buroc` (`subject`, `treatment`, `bstatus`, `DueDate`, `creatDate`, `notes`) VALUES
('בדיקת ימי מחלה לעובדים ומה תקרת הצבירה שיוכלו לצבור למשך השנה', 'להתקשר לאילנה ולברר אתה את הנושא ולהעביר מייל מסודר עם תשובתה לכלל העובדים', 4, '30/11/2016', '2016-11-15 10:39:45', 'הנושא עלה לאחר שאלות רבות מצד מספר עובדים שונים'),
('הוצאת עובדים לקורס פקחים', 'להתקשר לאמיר ולירון', 0, '2016-10-11', '2016-10-09 09:52:31', 'חשוב  מאוד'),
('חפיפת עובד ללימודי סגירת חודש', 'לקבוע עם חנניה זמנים שהוא והעובד יכולים', 1, '2016-10-26', '2016-10-09 10:15:05', 'לוודא עם חנניה שזה קורה בדיקת עדכון'),
('להוציא לקורס קופאים', 'להודיע לאמיר, לסגור תאריך', 0, '2016-10-27', '2016-10-18 20:29:39', 'חייב להתבצע בדחיפות'),
('לקדם את מכירת המנויים', 'בקשת תקציב מההנהלה', 1, '17/11/2016', '2016-11-06 22:02:38', 'חייב להתבצע');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
`id` int(11) NOT NULL,
  `receiver_name` varchar(50) NOT NULL,
  `receiver_email` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `attachment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `receiver_name`, `receiver_email`, `subject`, `content`, `attachment`) VALUES
(9, 'ליעד', 'eliraz680@gmail.com', 'ממשק חדש של המערכת לשליחת מיילים דרך השרת שלנו', 'מה שלומך? איך מרגישה? הכל טוב?\r\nתראי איזה יופי המערכת שלך יודעת לשלוח מיילים דרך האתר על חשבון שיצרתי בשרת שלנו.\r\nמגניב?\r\nלא אהבת? באסה.. באמת שניסיתי.\r\nחחח סתם זה מגניב ועובד יאללה תתחדשי שבת שלום.', NULL),
(10, 'liad', 'liadnj@gmail.com', 'העברת קובץ', '1', 'attachments/1482002661.sql'),
(11, 'liad', 'eliraz680@gmail.com', 'בדיקת שליחת קובץ במייל דרך המערכת', 'בדיקה', 'attachments/1482002780.pptx'),
(12, 'liad', 'liadnj@gmail.com', 'liadnj@gmail.com', 'fdf', NULL),
(13, 'liad', 'liadnj@gmail.com', 'הדגמה לשירן', 'הדגמה לשירן', 'attachments/1482009661.pptx'),
(14, 'liad', 'liadnj@gmail.com', 'liadnj@gmail.com', 'ffff', NULL),
(15, 'liad', 'liadnj@gmail.com', 'test2', 'שבוע טוב', NULL),
(16, 'liad', 'avivlevi231@gmail.com', 'fgdfg', 'dgdfh', NULL),
(17, 'liad', 'liadnj@gmail.com', 'liadnj@gmail.com', 'vbczf', NULL),
(18, 'liad', 'liadnj@gmail.com', 'liadnj@gmail.com', 'cgbcxgjkgjh', NULL),
(19, 'liad', 'liadnj@gmail.com', 'liadnj@gmail.com', 'dthfthfyhj', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `armed` int(11) NOT NULL,
  `Percent_of_jobs` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `color` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `cellphone`, `adress`, `role`, `armed`, `Percent_of_jobs`, `image`, `email`, `color`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2424, 'dgdg', 'dgdg', '49678676', 'ghhfhfghfg', 1, 2, 2, NULL, 'dgdg@ssd.com', '#00ff00', 1487408189, 1487408189, 300972973, 300972973),
(121212, 'color', 'color', '222222', 'color', 1, 1, 2, 'uploads/color.vsdx', 'color@color.com', '#76a5af', 1487328847, 1487328847, 300972973, 300972973),
(752752, 'ע5אטי', 'אע5אע', '5228528', 's3d3edses', 3, 2, 0, NULL, 'aaaa@aaa.con', '#1c4587', 1487488661, 1487488661, 300972973, 300972973),
(56222323, 'ינון', 'ניזרי', '0546669992', 'רח'' קרית יובל 28, ירושלים', 4, 1, 2, 'uploads/ינון.docx', 'yinon@yimon.com', '#f1c232', 1478539933, 1487330148, 255, 300972973),
(112299002, 'רם', 'בלאסן', '0549888110', 'רח'' עין כרם 56, ירושלים', 4, 3, 2, 'uploads/רם.png', 'ram@gmail.com', '#d5a6bd', 1478421704, 1487329909, 255, 300972973),
(203548746, 'אמרה', 'סלהו', '0546888899', 'רח'' הירקן 26, מעלה אדומים ', 3, 5, 3, 'uploads/אמרה.png', 'amare@amre.com', '#ff00ff', 1478421400, 1487330167, 255, 300972973),
(222995244, 'מעיין ', 'אוחיון', '02-9685742', 'רח'' המורדים 112, ירושלים', 5, 1, 0, 'uploads/מעיין .png', 'mayan@gmail.com', '#dd6a6a', 1478439185, 1487333133, 255, 300972973),
(252741963, 'בנימין', 'נתניהו', '02-9685237', 'רח'' המונולוג 9, ראשון לציון', 1, 3, 2, 'uploads/בנימין.jpg', 'nati@gmail.com', '#d9d9d9', 1478785418, 1487333096, 112299002, 300972973),
(283665218, 'אבי', 'אברהם', '04-8746696', 'רח'' הנרקיס 6, פתח תקוה', 1, 3, 3, 'uploads/אבי.jpg', 'avi@gmail.com', '', 1478716055, 1478785321, 112299002, 112299002),
(300972973, 'ליעד', 'ניזרי', '546220232', 'רח'' גבריהו חיים 7/1, ירושלים', 0, 0, 0, 'uploads/ליעד.sql', 'liadnj@gmail.com', '', 1478716055, 1481890639, 112299002, 300972973),
(300972974, 'אלירז', 'שמעון', '0524863254', 'רח'' אפרסמון, ירושלים', 0, 0, 0, 'uploads/אלירז.png', 'vhgunvfhh@dgf.com', '', 1477647441, 1478460005, 255, 255),
(305241788, 'יהודה', 'יששכר', '02-6585741', 'רח'' הפסגה 18, ירושלים', 3, 2, 2, 'uploads/יהודה.jpg', 'yhoh@gmail.com', '', 1478594215, 1478595859, 300972973, 112299002),
(305421968, 'מיכאל', 'מיכאל', '02-9685743', 'רח'' יהודה הנשיא, ירושלים', 4, 0, 0, 'uploads/מיכאל.png', 'michal@gmail.com', '#00ffff', 1478530046, 1487333007, 255, 300972973),
(693852741, 'אלעד', 'מזרחי', '029685741', 'רח'' המכבים, ירושלים', 3, 1, 0, 'uploads/אלעד.jpg', 'elad@gmail.com', '#f1c232', 1480842642, 1487332967, 112299002, 300972973),
(2147483647, 'asdasd', 'aasssaaa', '5455', 'ddddddd', 4, 0, 0, NULL, 'aaa@aaa.com', '#ffd966', 1487518776, 1487518776, 300972973, 300972973);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` varchar(100) NOT NULL,
  `employees` varchar(255) NOT NULL,
  `projects` varchar(255) NOT NULL,
  `holyday` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `team_leader` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `employees`, `projects`, `holyday`, `description`, `created_date`, `team_leader`) VALUES
('2017-02-01121212[0]', '121212', '', '', '', '2017-02-01', ''),
('2017-02-01203548746[2]', '203548746', '', '', '', '2017-02-01', ''),
('2017-02-0156222323[1]', '56222323', '', '', '', '2017-02-01', ''),
('2017-02-02112299002[2]', '112299002', '', '', '', '2017-02-02', ''),
('2017-02-02121212[0]', '121212', '', '', '', '2017-02-02', ''),
('2017-02-02203548746[3]', '203548746', '', '', '', '2017-02-02', ''),
('2017-02-02222995244[4]', '222995244', '', '', '', '2017-02-02', ''),
('2017-02-0256222323[1]', '56222323', '', '', '', '2017-02-02', ''),
('2017-02-03112299002[2]', '112299002', '', '', '', '2017-02-03', ''),
('2017-02-03121212[0]', '121212', '', '', '', '2017-02-03', ''),
('2017-02-03203548746[3]', '203548746', '', '', '', '2017-02-03', ''),
('2017-02-03222995244[4]', '222995244', '', '', '', '2017-02-03', ''),
('2017-02-03300972974[5]', '300972974', '', '', '', '2017-02-03', ''),
('2017-02-03305241788[6]', '305241788', '', '', '', '2017-02-03', ''),
('2017-02-03305421968[7]', '305421968', '', '', '', '2017-02-03', ''),
('2017-02-0356222323[1]', '56222323', '', '', '', '2017-02-03', ''),
('2017-02-03693852741[8]', '693852741', '', '', '', '2017-02-03', ''),
('2017-02-04112299002[1]', '112299002', '', '', '', '2017-02-04', ''),
('2017-02-04121212[0]', '121212', '', '', '', '2017-02-04', ''),
('2017-02-042147483647[5]', '2147483647', '', '', '', '2017-02-04', ''),
('2017-02-04300972974[2]', '300972974', '', '', '', '2017-02-04', ''),
('2017-02-04305241788[3]', '305241788', '', '', '', '2017-02-04', ''),
('2017-02-04305421968[4]', '305421968', '', '', '', '2017-02-04', ''),
('2017-02-05112299002[3]', '112299002', '', '', '', '2017-02-05', ''),
('2017-02-05121212[0]', '121212', '', '', '', '2017-02-05', ''),
('2017-02-05203548746[4]', '203548746', '', '', '', '2017-02-05', ''),
('2017-02-05305241788[5]', '305241788', '', '', '', '2017-02-05', ''),
('2017-02-0556222323[2]', '56222323', '', '', '', '2017-02-05', ''),
('2017-02-05693852741[6]', '693852741', '', '', '', '2017-02-05', ''),
('2017-02-05752752[1]', '752752', '', '', '', '2017-02-05', ''),
('2017-02-08752752[0]', '752752', '', '', '', '2017-02-08', '');

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE IF NOT EXISTS `invitations` (
  `item_name` varchar(200) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `open_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `due_date` varchar(100) NOT NULL,
  `quantity_order` int(200) NOT NULL,
  `approval_status` enum('מאושר','לא מאושר') NOT NULL,
  `order_status` enum('סופק','לא סופק') NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`item_name`, `supplier_name`, `open_date`, `due_date`, `quantity_order`, `approval_status`, `order_status`, `notes`) VALUES
('טאבלט למנויים', 'אודי כלים', '2016-11-06 19:04:50', '12/11/2016', 5, 'מאושר', 'סופק', ''),
('מברגת אימפקט', 'אודי כלים', '2016-11-10 13:49:41', '12/11/2016', 6, 'לא מאושר', 'סופק', ''),
('מברגת אימפקט', 'ביובית "פעיל" שירותי ביוב', '2016-11-09 08:12:45', '24/11/2016', 1, 'לא מאושר', 'לא סופק', 'her'),
('מברגת אימפקט', 'עמנואל שלם', '2016-11-07 17:34:32', '30/11/2016', 2, 'מאושר', 'לא סופק', 'לרכוש של חברת makita'),
('מלגזה', 'מחסן חנויות רשותי', '2016-11-06 19:06:06', '18/11/2016', 1, 'מאושר', 'סופק', ''),
('עטים', 'קרביץ', '2016-11-06 18:19:24', '19/11/2016', 100, 'לא מאושר', 'לא סופק', '');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `item_name` varchar(200) NOT NULL,
  `quantity_in_stock` int(100) NOT NULL,
  `price_each` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_name`, `quantity_in_stock`, `price_each`) VALUES
('טאבלט למנויים', 1, 2550),
('מברגת אימפקט', 2, 200),
('מלגזה', 2, 70000),
('עטים', 100, 3);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `author` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `title`, `description`, `created_date`, `author`) VALUES
(2, 'איחורים', 'מי שמאחר יותר מ10 דקות שיעשה פרסה ולא יבוא לעבודה', '2016-12-15 12:50:12', 'ליעד'),
(3, 'cgh', 'fhft', '2016-12-16 08:10:31', 'cghb'),
(4, 'xssf', 'dfgdfg', '2016-12-16 08:28:15', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1476869722),
('m140506_102106_rbac_init', 1476986097);

-- --------------------------------------------------------

--
-- Table structure for table `percent_of_jobs`
--

CREATE TABLE IF NOT EXISTS `percent_of_jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percent_of_jobs`
--

INSERT INTO `percent_of_jobs` (`id`, `name`) VALUES
(0, 'עד 40%'),
(2, 'עד 70%'),
(3, '100%');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(10) NOT NULL,
  `define_project` varchar(255) NOT NULL,
  `team_leader` varchar(255) NOT NULL,
  `employee` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due_date` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `define_project`, `team_leader`, `employee`, `location`, `start`, `due_date`, `notes`) VALUES
(5, 'ניקוי הבריכה', '300972974', '56222323,112299002,203548746,222995244', 'בריכה עליונה', '2016-12-27 10:20:49', '20/01/2017', 'לבדוק דלק למשורים'),
(6, 'גיזום עצים', '283665218', '112299002,203548746,222995244', 'כל השמורה', '2016-12-27 10:20:49', '15/11/2016', 'חייב להתבצע בדחיפות'),
(7, 'ניקיון השמורה', '300972973', '56222323,112299002,203548746', 'החלק המערבי', '2016-12-27 10:20:49', '23/11/2016', ''),
(8, 'סיור חשודים', '112299002', '56222323,222995244', 'החלק המזרחי', '2016-12-27 10:20:49', '07/11/2016', ''),
(9, 'סיור בטיחות', '203548746', '300972973,300972974,305421968', 'כל השמורה', '2016-12-27 10:20:49', '30/11/2016', ''),
(10, 'סוכה למציל', '300972974', '56222323, 112299002, 203548746, 222995244, 305241788', 'בריכת התמר', '2016-12-27 10:20:49', '11/11/2016', 'להשתמש בכפות של הסוכה מהחג'),
(11, 'ניקוי עלים', '252741963', '56222323, 112299002, 203548746, 222995244, 305241788, 305421968', 'צד מזרחי', '2016-12-27 10:20:49', '19/11/2016', ''),
(12, 'שיפוץ טרסות', '112299002', '112299002', 'עין פאת', '2016-12-27 10:20:49', '25/11/2016', 'חטיגאחעג'),
(13, 'צביעת החנות', '203548746', '56222323,112299002,203548746,222995244', 'כל השמורה', '2016-12-27 10:20:49', '12/12/2016', ''),
(14, 'ghfh', '252741963', '56222323, 203548746, 222995244', 'xxfv', '2016-12-27 10:20:49', '21/12/2016', 'sfvgfsgv'),
(15, 'erwe', '252741963', '56222323,112299002,203548746,222995244,252741963,283665218,300972973,300972974,305241788,305421968,693852741', 'ee', '2016-12-27 10:20:49', '14/12/2016', 'rere'),
(16, 'sgdfg', '300972973', '203548746, 222995244, 252741963, 283665218', 'fgdg', '2016-12-27 10:20:49', '15/12/2016', 'dggd'),
(17, 'fgxbgcfgb', '252741963', '112299002, 252741963, 283665218, 300972973', 'fdgd', '2016-12-27 10:23:32', '22/12/2016', 'dfgdf'),
(18, 'בנסבניבסע', '252741963', '56222323, 203548746, 252741963, 300972973, 305241788, 305421968', ' יהיח', '2017-01-16 08:51:06', '28/12/2016', 'החהבח');

-- --------------------------------------------------------

--
-- Table structure for table `revenues`
--

CREATE TABLE IF NOT EXISTS `revenues` (
  `date` varchar(100) NOT NULL,
  `day` varchar(255) DEFAULT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `revenues`
--

INSERT INTO `revenues` (`date`, `day`, `cash_desk_784`, `cash_desk_782`, `store`) VALUES
('2012-01-01', 'ראשון', 3854, 4521, 13052),
('2012-02-01', 'רביעי', 9374, 3333, 5520),
('2012-03-01', 'חמישי', 5632, 4521, 570),
('2012-04-01', 'שני', 478, 362, 4000),
('2012-05-01', 'שלישי', 3524, 2541, 500),
('2012-06-01', 'שישי', 3341, 2564, 300),
('2012-07-01', 'ראשון', 2412, 2000, 300),
('2012-08-01', 'ראשון', 3524, 2569, 2361),
('2012-09-01', 'שבת', 2712, 2541, 360),
('2012-10-01', 'שני', 1256, 9635, 2145),
('2012-11-01', 'חמישי', 3654, 3652, 6856),
('2012-12-01', 'שבת', 3624, 9638, 102),
('2013-01-01', 'ראשון', 3547, 2564, 203),
('2013-02-01', 'שני', 3541, 695, 2364),
('2013-03-01', 'שני', 2785, 2365, 365),
('2013-04-01', 'שלישי', 1589, 251, 3694),
('2013-05-01', 'שני', 3985, 695, 5211),
('2013-06-01', 'שבת', 5745, 3652, 8547),
('2013-07-01', 'שני', 3685, 6985, 1244),
('2013-08-01', 'ראשון', 2785, 2541, 635),
('2013-09-01', 'ראשון', 3652, 254, 632),
('2013-10-01', 'ראשון', 6352, 521, 854),
('2013-11-01', 'רביעי', 8521, 201, 100),
('2013-12-01', 'ראשון', 4185, 213, 352),
('2014-01-01', 'שלישי', 6354, 254, 362),
('2014-02-01', 'ראשון', 4178, 365, 412),
('2014-03-01', 'שני', 6389, 65, 63),
('2014-04-01', 'רביעי', 6387, 632, 521),
('2014-05-01', 'רביעי', 9638, 21, 365),
('2014-06-01', 'שלישי', 6385, 63, 254),
('2014-07-01', 'רביעי', 9638, 251, 362),
('2014-08-01', 'רביעי', 1423, 6544, 213),
('2014-09-01', 'שלישי', 3654, 213, 365),
('2014-10-01', 'רביעי', 6352, 362, 542),
('2014-11-01', 'רביעי', 4521, 354, 987),
('2014-12-01', 'שלישי', 5241, 213, 365),
('2015-01-01', 'חמישי', 797, 365, 987),
('2015-01-02', 'שלישי', 607, 354, 700),
('2015-01-03', 'שבת', 1949, 41, 177),
('2015-01-04', 'רביעי', 58, 2541, 3652),
('2015-01-05', 'רביעי', 146, 2634, 200),
('2015-01-06', 'שלישי', 982, 2541, 310),
('2015-01-07', 'שלישי', 116, 9541, 26),
('2015-01-08', 'שלישי', 261, 2541, 3521),
('2015-02-01', 'ראשון', 7854, 25, 254),
('2015-03-01', 'שלישי', 6589, 3521, 21),
('2015-04-01', 'ראשון', 5412, 2541, 3652),
('2015-05-01', 'שני', 6352, 2511, 6568),
('2015-06-01', 'ראשון', 3214, 3652, 22),
('2015-07-01', 'שלישי', 4152, 241, 3522),
('2015-08-01', 'שבת', 7485, 355, 9856),
('2015-09-01', 'שלישי', 7486, 254, 365),
('2015-10-01', 'חמישי', 9658, 365, 254),
('2015-11-01', 'שלישי', 5412, 254, 365),
('2015-12-01', 'שלישי', 6352, 222, 22),
('2016-01-01', 'שלישי', 2587, 225, 355),
('2016-02-01', 'שלישי', 6385, 74, 245),
('2016-03-01', 'שני', 9874, 22, 524),
('2016-04-01', 'שני', 1236, 36, 666),
('2016-05-01', 'שלישי', 5632, 222, 6352),
('2016-06-01', 'רביעי', 6374, 222, 352),
('2016-07-01', 'שישי', 6541, 22, 369),
('2016-08-01', 'שלישי', 6398, 22, 355),
('2016-09-01', 'חמישי', 8569, 214, 6532),
('2016-11-16', 'רביעי', 541, 875, 60),
('2016/10/12', 'שלישי', 2544, 635, 632),
('2016/12/01', 'שני', 6987, 236, 541),
('2017/1/03', 'שלישי', 6426, 365, 854),
('2017/10/11', 'שני', 6352, 235, 32),
('2017/11/03', 'שלישי', 6395, 522, 74),
('2017/11/24', 'שני', 3698, 2147, 213),
('2017/12/09', 'שני', 6985, 632, 3215),
('2017/12/27', 'שלישי', 6321, 5421, 85),
('2017/12/28', 'רביעי', 3638, 325, 85),
('2017/2/08', 'רביעי', 2365, 36, 98),
('2017/2/17', 'ראשון', 6352, 85, 96),
('2017/3/03', 'ראשון', 5632, 65, 89),
('2017/3/08', 'שלישי', 652, 235, 14),
('2017/4/14', 'חמישי', 6985, 35, 14),
('2017/4/20', 'שני', 963, 254, 23),
('2017/5/12', 'שני', 6352, 412, 85),
('2017/5/18', 'ראשון', 632, 235, 98),
('2017/6/15', 'שלישי', 9688, 32, 23),
('2017/6/17', 'ראשון', 69, 855, 21),
('2017/7/06', 'שני', 2938, 32, 51),
('2017/7/12', 'שלישי', 2638, 52, 47),
('2017/8/19', 'חמישי', 9635, 325, 875),
('2017/8/20', 'שלישי', 2541, 5231, 87),
('2017/9/01', 'שלישי', 9685, 325, 411);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(0, 'ראש צוות - פקח - נשק'),
(1, 'ראש צוות - פקח'),
(2, 'ראש צוות'),
(3, 'פקח'),
(4, 'עובד - קופאי'),
(5, 'עובד'),
(6, 'שומר לילה');

-- --------------------------------------------------------

--
-- Table structure for table `schedual`
--

CREATE TABLE IF NOT EXISTS `schedual` (
  `month` varchar(100) CHARACTER SET utf8 NOT NULL,
  `employeesName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `days` varchar(255) CHARACTER SET utf8 NOT NULL,
`id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedual`
--

INSERT INTO `schedual` (`month`, `employeesName`, `days`, `id`) VALUES
('6', 'liad', '06/12/2016', 1),
('3', 'liad', '23/12/2016', 2),
('4', 'liad', '21/11/2016', 3),
('2', 'liad', '21/01/2017', 5),
('9', 'liad', '03/01/2017,04/01/2017,17/01/2017,12/01/2017,09/01/2017,10/01/2017', 6),
('3', 'liad', '23/01/2017,09/01/2017,26/01/2017,19/01/2017,11/01/2017,17/01/2017,15/01/2017,12/01/2017', 7),
('3', 'admin', '23/01/2017,13/01/2017,10/01/2017,25/01/2017,08/01/2017,19/01/2017', 8),
('7', 'admin', '16/01/2017,11/01/2017,26/01/2017,20/01/2017,08/01/2017', 9),
('3', 'liad', '14/02/2017', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
`id` int(100) NOT NULL,
  `phonenum` varchar(255) NOT NULL,
  `smssubject` varchar(255) NOT NULL,
  `smsmes` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `phonenum`, `smssubject`, `smsmes`) VALUES
(1, '434', '4456', '46346');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`date`, `day`, `cash_desk_784`, `cash_desk_782`, `store`) VALUES
('2012-01-01', 'א', 30, 30, 10),
('2012-02-01', 'ד', 60, 5, 4),
('2012-03-01', 'ה', 40, 50, 5),
('2012-04-01', 'א', 100, 40, 5),
('2012-05-01', 'ג', 100, 80, 2),
('2012-06-01', 'ו', 200, 10, 5),
('2012-07-01', 'א', 200, 50, 2),
('2012-08-01', 'ד', 200, 60, 5),
('2012-09-01', 'ש', 200, 30, 3),
('2012-10-01', 'ב', 100, 80, 3),
('2012-11-01', 'ה', 100, 30, 9),
('2012-12-01', 'ש', 40, 50, 6),
('2013-01-01', 'ש', 1, 0, 1),
('2013-02-01', 'ג', 4, 2, 2),
('2013-03-01', 'ג', 25, 25, 7),
('2013-04-01', 'ה', 50, 50, 13),
('2013-05-01', 'ו', 50, 50, 70),
('2013-06-01', 'ב', 200, 10, 10),
('2013-07-01', 'ש', 200, 40, 8),
('2013-08-01', 'ב', 200, 40, 1),
('2013-09-01', 'ד', 199, 2, 1),
('2013-10-01', 'א', 100, 40, 1),
('2013-11-01', 'ג', 40, 40, 6),
('2013-12-01', 'ו', 10, 10, 5),
('2014-01-01', 'ד', 4, 4, 1),
('2014-02-01', 'ש', 2, 2, 2),
('2014-03-01', 'ש', 20, 10, 5),
('2014-04-01', 'ג', 40, 40, 8),
('2014-05-01', 'ה', 100, 30, 5),
('2014-06-01', 'א', 100, 69, 1),
('2014-07-01', 'ג', 100, 80, 6),
('2014-08-01', 'ג', 100, 70, 9),
('2014-09-01', 'ב', 100, 40, 3),
('2014-10-01', 'ד', 30, 30, 30),
('2014-11-01', 'ש', 10, 20, 9),
('2014-12-01', 'ב', 5, 3, 2),
('2015-01-01', 'ה', 10, 10, 19),
('2015-02-01', '6', 20, 20, 2),
('2015-03-01', 'ש', 20, 30, 7),
('2015-04-01', 'א', 40, 40, 5),
('2015-05-01', 'ב', 100, 10, 9),
('2015-06-01', 'ג', 100, 50, 2),
('2015-07-01', 'ד', 100, 69, 1),
('2015-08-01', 'ה', 100, 60, 6),
('2015-09-01', 'א', 100, 40, 2),
('2015-10-01', 'א', 100, 2, 1),
('2015-11-01', 'ד', 30, 30, 6),
('2015-12-01', 'ו', 20, 20, 8),
('2016-01-01', 'ו', 20, 9, 1),
('2016-02-01', 'ב', 5, 5, 10),
('2016-03-01', 'ג', 20, 3, 2),
('2016-04-01', 'ו', 30, 5, 30),
('2016-05-01', 'א', 40, 40, 5),
('2016-06-01', 'ד', 100, 20, 2),
('2016-07-01', 'ו', 100, 20, 30),
('2016-08-01', 'ב', 100, 30, 6),
('2016-09-01', 'ה', 100, 20, 3),
('2016-10-01', 'ה', 20, 20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `summaryday`
--

CREATE TABLE IF NOT EXISTS `summaryday` (
  `date` varchar(255) NOT NULL,
  `israels` int(255) DEFAULT NULL,
  `tourist` int(255) DEFAULT NULL,
  `matmon` int(255) DEFAULT NULL,
  `events` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `summaryday`
--

INSERT INTO `summaryday` (`date`, `israels`, `tourist`, `matmon`, `events`, `notes`) VALUES
('01/11/2016', 91, 5, 100, 'ללא', ''),
('01/12/2016', 33, 46, 99, 'ללא', ''),
('02/11/2016', 33, 55, 56, 'ללא', '\r\n'),
('06/12/2016', 52, 63, 54, 'ללא', ''),
('10/11/2016', 720, 65, 9, 'ללא', '\r\n\r\n\r\nghkhk\r\n\r\n\r\n\r\nhkfh'),
('30/11/2016', 100, 100, 12, 'ללא ארועים מיוחדים', 'בעיהי');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_name` varchar(100) NOT NULL,
  `phoneNum` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `contactPhone` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_name`, `phoneNum`, `address`, `contact`, `contactPhone`, `category`, `notes`) VALUES
('אודי כלים', '02-6985417', 'רח'' החבצלת 29', 'נחמה', '03-9685741', 'כלי עבודה', ''),
('אלמוג לבנין', '0524713525', 'רח'' בית הדפוס, ירושלים', 'אלמוג', '0583264152', 'בניין', ''),
('אסם שיווק', '0528749652', 'רח'' הנרקיס 3, ירושלים', 'איציק', '02-9685742', 'מזון', ''),
('ביובית "פעיל" שירותי ביוב', '02-6565777', 'רח'' רמה 19 , גבעת זאב', 'דרור', '505305757', 'כלי עבודה', ''),
('מחסן חנויות רשותי', '02-9685321', 'רח'' עם ועולמו 3', 'שמעון', '05287413', 'כלי עבודה', ''),
('מחסני חשמל', '09-6857451', 'רח'' התירוש 85', 'רבין', '0548734982', 'מוצרי חשמל', ''),
('עמנואל שלם', '02-9685471', 'רח'' החלוץ 9, ירושלים', 'יוסי', '0548973621', 'בניין', ''),
('קלור מוצרי נקיון', '029685741', 'רח'' האודם 251, ירושלים', 'רוני', '0528741396', 'מוצרי ניקוי', ''),
('קרביץ', '02-6985471', 'רח'' בית הדפוס, ירושלים', 'מנחם', '02-5871413', 'מכשירי כתיבה', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(254, 'teamleader', '$2y$13$3Pvg9yLywuxbKfqcXTKecOgmaIcRGHle66xQm782bXZnxzknGa5S6', 'WnVA1xkwvdC5AT069FcKCwDE2r9UPlQa', 1477560874, 1477647177, NULL, 255),
(255, 'admin', '$2y$13$CSBZcZvsV0f4DATidh4Cvuw8t2a8W02/RsvXB/LoyOASkrstoxxjS', '7O_FUKnntTMBo-jXxks_H9rCcYr9Ta5j', 1477570055, 1477647122, NULL, 255),
(56222323, 'inon', '$2y$13$bN7qOsWL5F8ro0cW/o4.Z.XHK1puSoFOpVNHNSdjOqn7lkPwWhw9e', 'v7kRyjyq_wdqGoDEsTh67KFOfkXiUe2D', 1481798905, 1481798905, 300972973, 300972973),
(112299002, 'admin', '$2y$13$eaxrhQF/OBacGe0MWoH69.Way7/Aky5G5rrGP1p7RDcsY5hgdNHHe', 'Xj2CiUxqQ2EkW8R2d5aREYCEiJz9Z6h3', 1478551871, 1481798850, 203548746, 112299002),
(203548746, 'liozr', '$2y$13$Fv.hOkpxsodA3B0CpR94XuewquIYj1pDnVeRWQfa2uIJwQEjtS98.', 'Bk40WBBJPM26Jk-z-LxozSoTcCVM9yuT', 1478676905, 1478676905, 112299002, 112299002),
(222995244, 'teamleader', '$2y$13$XcgfaKTqvl8MHdxYcDlRqOFwcPiDa3fcun.DxriqtKF5CC/9U5wCy', '6mz3B2jH97yPO9ElBBIURoJGYJeC3u-p', 1478551953, 1478551953, 112299002, 112299002),
(252741963, 'bibi', '$2y$13$3LpgGrckpQCFoVn00DXKhu7V0GLJlxzpryA2ZZzxQHCs8taqLt.zS', '66o4AhgC6VLeBCFcD41wcJve9HHUpGBn', 1481798933, 1481798933, 300972973, 300972973),
(300972973, 'liad', '$2y$13$KVFpB6.YiZ2R31EtTMR80eWsdtEkb86i.2YE2fgvPzBtxPFC1qwca', 'F_iGL7dtHW-SYAitZYVEc_Bwt948Gugu', 1478550154, 1478550154, 203548746, 203548746),
(300972974, 'employee', '$2y$13$wMfE3u3n0mcx8Xtnrn3VHedM5/Qhi0k2AEH5y/CoOUFXeL6xKTMUy', 'pehIu5RJlsjjI2alP4HdCPNZX08TB4pU', 1477644374, 1477644374, 255, 255),
(305421968, 'employee', '$2y$13$4cSFt.0wsz4FwE1nihrMbeRon0./BneV7s9wnR9DN3bHwoz/1Xtdy', 'i9jBWmc1i79UdJC18c-bYiE4BRpAz5V9', 1478552345, 1478552345, 112299002, 112299002);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE IF NOT EXISTS `visitors` (
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`date`, `day`, `cash_desk_784`, `cash_desk_782`, `store`) VALUES
('2012-01-01', 'שני', 154, 11, 11),
('2012-02-01', 'שלישי', 374, 25, 522),
('2012-03-01', 'שלישי', 963, 36, 25),
('2012-04-01', 'ראשון', 478, 22, 36),
('2012-05-01', 'שלישי', 524, 36, 55),
('2012-06-01', 'שישי', 341, 50, 46),
('2012-07-01', 'שני', 412, 20, 15),
('2012-08-01', 'שלישי', 524, 63, 21),
('2012-09-01', 'ראשון', 712, 32, 20),
('2012-10-01', 'שלישי', 256, 63, 52),
('2012-11-01', 'רביעי', 654, 244, 63),
('2012-12-01', 'ראשון', 624, 52, 63),
('2013-01-01', 'שני', 547, 63, 58),
('2013-02-01', 'חמישי', 2541, 21, 25),
('2013-03-01', 'שבת', 4785, 63, 85),
('2013-04-01', 'שני', 589, 21, 3),
('2015-01-01', 'שני', 20, 632, 22),
('2015-01-02', 'ראשון', 65, 96, 21),
('2015-01-03', 'שבת', 63, 521, 632),
('2015-01-04', 'ראשון', 632, 520, 632),
('2015-01-05', 'שני', 254, 35, 62),
('2015-01-06', 'שני', 87, 100, 40),
('2015-01-07', 'שני', 150, 12, 13),
('2015-01-08', 'שלישי', 256, 36, 25),
('2015-02-01', 'ראשון', 692, 526, 32),
('2015-03-01', 'ראשון', 285, 36, 14),
('2015-04-01', 'ראשון', 252, 50, 36),
('2015-05-01', 'ראשון', 360, 47, 14),
('2015-06-01', 'שני', 36, 870, 24),
('2015-07-01', 'רביעי', 69, 420, 36),
('2015-08-01', 'שלישי', 69, 150, 32),
('2015-09-01', 'שלישי', 360, 44, 41),
('2015-10-01', 'חמישי', 320, 47, 53),
('2015-11-01', 'ראשון', 630, 21, 32),
('2015-12-01', 'שלישי', 320, 21, 32);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `armed`
--
ALTER TABLE `armed`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `bstatus`
--
ALTER TABLE `bstatus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buroc`
--
ALTER TABLE `buroc`
 ADD PRIMARY KEY (`subject`), ADD KEY `status` (`bstatus`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
 ADD PRIMARY KEY (`id`), ADD KEY `role` (`role`), ADD KEY `armed` (`armed`), ADD KEY `Percent_of_jobs` (`Percent_of_jobs`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
 ADD PRIMARY KEY (`item_name`,`supplier_name`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`item_name`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `percent_of_jobs`
--
ALTER TABLE `percent_of_jobs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD KEY `employee` (`employee`), ADD KEY `employee_2` (`employee`), ADD KEY `employee_3` (`employee`);

--
-- Indexes for table `revenues`
--
ALTER TABLE `revenues`
 ADD PRIMARY KEY (`date`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedual`
--
ALTER TABLE `schedual`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
 ADD PRIMARY KEY (`date`);

--
-- Indexes for table `summaryday`
--
ALTER TABLE `summaryday`
 ADD PRIMARY KEY (`date`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`supplier_name`), ADD UNIQUE KEY `supplier_name` (`supplier_name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
 ADD PRIMARY KEY (`date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `schedual`
--
ALTER TABLE `schedual`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `buroc`
--
ALTER TABLE `buroc`
ADD CONSTRAINT `bstatus` FOREIGN KEY (`bstatus`) REFERENCES `bstatus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
ADD CONSTRAINT `Percent_of_jobs` FOREIGN KEY (`Percent_of_jobs`) REFERENCES `percent_of_jobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `armed` FOREIGN KEY (`armed`) REFERENCES `armed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `role` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
