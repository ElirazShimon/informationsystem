<?php

namespace app\controllers;

use Yii;
use app\models\Invitations;
use app\models\InvitationsSearch;
use app\models\Suppliers;
use app\models\Item;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use yii\filters\AccessControle;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * InvitationsController implements the CRUD actions for Invitations model.
 */
class InvitationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invitations models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (!\Yii::$app->user->can('indexInvitations'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $searchModel = new InvitationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'supplier_nameS' => Suppliers::getSupplierssWithAllSupplierss(),
			'supplier_name' => $searchModel->supplier_name,
			'item_names' => Item::getItemWithAllItem(),
			'item_name' => $searchModel->item_name,
        ]);
    }

	public function actionExport()
    {
		$invitationsExport= Invitations::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $invitationsExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['item_name','supplier_name','open_date','due_date','quantity_order','approval_status','order_status','notes'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['item_name' => 'שם פריט', 'supplier_name' => 'שם ספק','open_date' => 'תאריך פתיחה', 'due_date' => 'תאריך אספקה', 'quantity_order' => 'כמות', 'approval_status' => 'אישור הזמנה', 'order_status' => 'סטטוס הזמנה','notes'=>'הערות'], 
			'fileName' => 'Invitations',
	
		]);
	}
    /**
     * Displays a single Invitations model.
     * @param string $item_name
     * @param string $supplier_name
     * @return mixed
     */
    public function actionView($item_name, $supplier_name)
    {
		if (!\Yii::$app->user->can('viewInvitations'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        return $this->render('view', [
            'model' => $this->findModel($item_name, $supplier_name),
        ]);
    }

    /**
     * Creates a new Invitations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (!\Yii::$app->user->can('createInvitations'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = new Invitations();
		
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		//מקבל מופע של העלאת הקובץ
		//מקבל מופע של העלאת הקובץ
			$imageName = date("Y-m-d H:i:s");
			$model->file = UploadedFile::getInstance($model,'file');
			$model->file->saveAs('web/uploads/'.$imageName.'.'.$model->file->extension);

			//שומר את הנתיב בדאטא בייס
			$model->image ='uploads/'.$imageName.'.'.$model->file->extension; 
			
			$model->save();
            
            
            return $this->redirect(['view', 'item_name' => $model->item_name, 'supplier_name' => $model->supplier_name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Invitations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $item_name
     * @param string $supplier_name
     * @return mixed
     */
    public function actionUpdate($item_name, $supplier_name)
    {
		if (!\Yii::$app->user->can('updateInvitations'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = $this->findModel($item_name, $supplier_name);

        if ($model->load(Yii::$app->request->post()) ) 
		{
			//מקבל מופע של העלאת הקובץ
			$bidName = date("Y-m-d H:i:s");
			$model->Bid = UploadedFile::getInstance($model,'Bid');
			$model->Bid->saveAs('הצעות_מחיר/'.$bidName.'.'.$model->Bid->extension);
			
			//שומר את הנתיב בדאטא בייס
			$model->Bid ='הצעות_מחיר/'.$bidName.'.'.$model->file->extension; 
			
			
			$model->save();
            return $this->redirect(['view', 'item_name' => $model->item_name, 'supplier_name' => $model->supplier_name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invitations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $item_name
     * @param string $supplier_name
     * @return mixed
     */
    public function actionDelete($item_name, $supplier_name)
    {
		if (!\Yii::$app->user->can('deleteInvitations'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $this->findModel($item_name, $supplier_name)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invitations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $item_name
     * @param string $supplier_name
     * @return Invitations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($item_name, $supplier_name)
    {
        if (($model = Invitations::findOne(['item_name' => $item_name, 'supplier_name' => $supplier_name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
