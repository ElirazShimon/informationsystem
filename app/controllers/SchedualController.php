<?php

namespace app\controllers;

use Yii;
use app\models\Schedual;
use yii\data\SqlDataProvider;
use app\models\SchedualSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SchedualController implements the CRUD actions for Schedual model.
 */
class SchedualController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedual models.
     * @return mixed
     */
	 public function actionMonths()
    {
		
        $events = \app\models\Events::find()->all();
		$tasks = [];
		
		
		
        foreach ($events as $eve) 
        {
			$tmoora = false;
			$mahala = false;
			$hofesh = false;
			
		  if (strpos($eve->employees, 'T') !== false) {
		  $tmoora = true;
		  }
		  if (strpos($eve->employees, 'M') !== false) {
		  $mahala = true;
		  }
		  if (strpos($eve->employees, 'H') !== false) {
		  $hofesh = true;
		  }
          $event = new \yii2fullcalendar\models\Event();
		  $color = \app\models\Employees::findOne($eve->employees)->color;
          $event->backgroundColor = 'white';
		  $event->borderColor = $color;
		  $event->className = 'btn';
		  $event->id = "$eve->created_date,$eve->employees";
		  //$eve->employees=explode(', ',$eve->employees);//converting to array...
		  $counter = count($eve->employees);
         // $event->title = "$eve->employees  $eve->created_date";
		   //$event->title = $eve->employees[0];
			
		  $name = \app\models\Employees::findOne($eve->employees)->fullname;
		  $role = \app\models\Employees::findOne($eve->employees)->roleItem->name;
		  $event->title = "$name";
		  if($tmoora)
		  $event->title = " $name  - יום תמורה";
		  if($mahala)
		  $event->title = " $name  - יום מחלה";
		  if($hofesh)
		  $event->title = " $name  - יום חופש";
		  
		  if("$name" == "$eve->team_leader"){
		  $event->title = "$name - ראש צוות";
		  }
		  $resultCashier = explode(',',$eve->cashier );
		  for ($c = 0; $c <sizeof($resultCashier) ; $c++){
		  if("$eve->employees" == "$resultCashier[$c]")
		   $event->title = "$name - קופאי";
		  }
		  $event->start = $eve->created_date;
          $tasks[] = $event;
		  }
		  
		   return $this->render('months', [
		
            'events' => $tasks,
        ]);
    }
	
	
    public function actionIndex()
    {
	 if (\Yii::$app->user->can('createUser')) { 
		$searchModel = new SchedualSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		} 
		
		
		 if (!\Yii::$app->user->can('createUser')){
		$UserName = Yii::$app->user->identity->username;
        $searchModel = new SchedualSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//'.$UserName.'
		$UserName = Yii::$app->user->identity->username;
		$dataProvider = new SqlDataProvider([
      'sql' => 'SELECT * FROM `schedual` WHERE `employeesName` = "'.$UserName.'" ORDER BY `schedual`.`month` ASC',
     // 'totalCount' => $count,
      
      'sort' => [
         'attributes' => [
            'employeesName',
            'days',
			
           
         ],
      ],
   ]);
		}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedual model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schedual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedual();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
		/////////////////////////////////////////////////////////////
		
		
		/*$model = new Schedual();
                if(isset($_POST['Schedual']))
                {
                        $model->attributes=$_POST['Schedual'];
                        if($model->days!=='')
                                $model->days=implode(', ',$model->days);//converting to string...
                        if($model->save())
                                $this->redirect(array('view','id'=>$model->id));
                }
				   else {
            return $this->render('create', [
                'model' => $model,
            ]);
               $model->days=explode(', ',$model->days);//converting to array...
                $this->render('create',array(
                        'model'=>$model,
                ));
        }*/
		
    }

    /**
     * Updates an existing Schedual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->month]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schedual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Schedual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedual::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
