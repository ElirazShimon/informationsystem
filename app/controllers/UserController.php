<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		//if (!\Yii::$app->user->can('indexUser') && 
		  //  !\Yii::$app->user->can('indexOwnUser') )
			//throw new UnauthorizedHttpException ('Hey, You are not allowed to update users'); 
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
        ]);
    }

	public function actionExport()
    {
		$userExport= User::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $userExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','username'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'ת.ז', 'username' => 'שם משתמש'], 
			'fileName' => 'Users',
	
		]);
	}
	
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$model = $this->findModel($id);
		if (!\Yii::$app->user->can('viewUser') && 
		    !\Yii::$app->user->can('viewOwnUser', ['user' =>$model]) )
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       //access control
		if (!\Yii::$app->user->can('createUser'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		$model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles();
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		if (!\Yii::$app->user->can('updateUser') && 
		    !\Yii::$app->user->can('updateOwnUser', ['user' =>$model]) )
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles();
            return $this->render('update', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteUser'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
  /*  protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }*/
 protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
			//$roles = \Yii::$app->authManager->getRolesByUser($model->id);
			//$model->role = array_keys($roles)[0];
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	}
