<?php

namespace app\controllers;

use Yii;
use app\models\Visitors;
use app\models\VisitorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * VisitorsController implements the CRUD actions for Visitors model.
 */
class VisitorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Visitors models.
     * @return mixed
     */
    public function actionIndex()
    {
	if (!\Yii::$app->user->can('indexVisitors'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $searchModel = new VisitorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionExport()
    {
		$visitorsExport= Visitors::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $visitorsExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['date','day','cash_desk_784','cash_desk_782','store'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['date' => 'תאריך', 'day' => 'יום','cash_desk_784' => 'קופה 784', 'cash_desk_782' => 'קופה 782', 'store' => 'חנות'], 
			'fileName' => 'Visitors',
	
		]);
	}
	
	public function actionChart1()
	 {
		 return $this->render('chart1');
	 }
	 
    /**
     * Displays a single Visitors model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
		if (!\Yii::$app->user->can('viewVisitors'))
		throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Visitors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (!\Yii::$app->user->can('createVisitors'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = new Visitors();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->date]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Visitors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (!\Yii::$app->user->can('updateVisitors'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->date]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Visitors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteVisitors'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visitors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Visitors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visitors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
