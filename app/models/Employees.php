<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use yii\base\Model;
/**
 * This is the model class for table "employees".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $role
 * @property integer $Percent_of_jobs
 * @property integer $cellphone
 * @property string $adress
 * @property string $armed
 *
 * @property Role $role0
 */
class Employees extends \yii\db\ActiveRecord
{
    
	
	
	public static function tableName()
    {
        return 'employees';
    }
/**
* access url
**/
	
	
	  
	
	
 
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'first_name', 'last_name', 'role', 'Percent_of_jobs', 'cellphone', 'adress', 'armed'], 'required','message'=>'שדה חובה'],
            [['id', 'role', 'Percent_of_jobs', 'created_at','updated_at','created_by','updated_by'], 'integer','message'=>'ספרות בלבד'],
			['email', 'email'],
			[['image'],'file'],
			[['files'],'file'],
            [['first_name', 'last_name', 'adress', 'armed','image','cellphone','color'], 'string', 'max' => 255],
            
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ת.ז',
            'first_name' => 'שם פרטי',
            'last_name' => 'שם משפחה',
            'role' => 'תפקיד',
            'Percent_of_jobs' => 'אחוזי משרה',
            'cellphone' => 'טלפון',
            'adress' => 'כתובת',
			'email'  => 'מייל',
            'armed' => 'נשק',
			'image' => 'תמונה',
			'color' => 'צבע',
			'files' => 'קבצי עובד',
			'created_at' => 'created_at',
			'updated_at' => 'updated_at',
			'created_by' => 'created_by',
			'updated_by' => 'updated_by',
        ];
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getColor()
    {
        return $this->color;
    }
	
	public static function getEmail()
    {
       return $this->email;		
    }
	
	public function getFullname()
    {
        return $this->first_name.' '.$this->last_name;
    }
	
	public function getId()
    {
        return $this->id;
    }
	
	public static function getEmployees() //function that give employee's names
	{
		$allEmployees = self::find()->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'id', 'fullname');
		return $allEmployeesArray;						
	}
	
	public static function getEmployeesID($fullname) //function that give employee's names
	{
		$arrfullname = explode(' ',$fullname);
		$IDEmployees = self::find()->where(['first_name' => "$arrfullname[0]",'last_name'=>"$arrfullname[1]"])->one();
		
		return  $IDEmployees->id;						
	}
	
	public static function getEmployeesPerDays($employeesForDay)
	{
		$allEmployees = self::find()->where(['id' => $employeesForDay])->all();
		
		$allEmployeesArray = ArrayHelper::
					map($allEmployees,'id' , 'fullname');
		return $allEmployeesArray;						
	}

	public static function getTeamLeaderTprojects($id) //send fullname of team_leade to project
	{
		$ProjectTeamLeader = self::find()->where(['id' => $id])->one();
		
		  return $ProjectTeamLeader->fullname;
	}
	
	
	public static function getEmployeesToUsers($employeesForDay)
	{
		$allEmployees = self::find()->where(['id' => $employeesForDay])->all();
		
		$allEmployeesArray = ArrayHelper::
					map($allEmployees,'id' , 'fullname');
		return $allEmployeesArray;						
	}
	
	public static function getEmployeesEmails() //function that give all employee's email
	{
		$allEmployees = self::find()->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'id', 'email');
		return $allEmployeesArray;						
	}
	
	public static function getEmployeesEmailsToProjects($id) //send email employee to project
	{
	$allEmployees = self::find()->where(['id' => $id])->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'id', 'email');
		return $allEmployeesArray;						
	}
	
	public function getfile()
    {
        return $this->hasMany(ImageManager::className(), ['item_id' => 'id']);
    }

	
	
	public function getRoleItem()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
	
	public function getArmedItem()
    {
        return $this->hasOne(Armed::className(), ['id' => 'armed']);
    }
	
	public function getPercent_of_jobsItem()
    {
        return $this->hasOne(PercentOfJobs::className(), ['id' => 'Percent_of_jobs']);
    }
	
	public function getUserItem()
    {
        return $this->hasMany(User::className(), ['id' => 'id']);
    }

	public function getProjectEmployee()
    {
        return $this->hasMany(Projects::className(), ['employee' => 'id']);
    }
    
	public function getProjectTeamleder()
    {
        return $this->hasOne(Projects::className(), ['team_leade' => 'id']);
    }


public static function getEproject()
	{
		$allEproject = self::find()->all();
		$allEprojectArray = ArrayHelper::
					map($allEproject, 'id', 'fullname');
		return $allEprojectArray;						
	}
	
	public static function getEprojectWithAllEproject()
	{
		$allEproject= self::getEproject();
		$allEproject[-1] = 'כל סוגי הנשק';
		$allEproject = array_reverse ( $allEproject, true );
		return $allEproject;	
	}		

	}