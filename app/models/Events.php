<?php

namespace app\models;

use Yii;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;
use app\models\Employees;
use app\models\Emails;
use yii\helpers\ArrayHelper;	


/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $employees
 * @property string $projects
 * @property string $holyday
 * @property string $description
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employees'], 'required'],
            [[ 'projects', 'holyday', 'description','month','team_leader','cashier'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees' => 'עובדים',
            'projects' => 'פרויקטים',
            'holyday' => 'חגים',
            'description' => 'הערות',
			 'month' => 'חודש',
			'team_leader' => 'ראש צוות',
			'created_date' => 'שיבוץ ליום',
			'cashier'=> 'צוות קופה',
        ];
    }
	/*public static function getMonth($monthName)
	{
	//$month = $monthName;
	return $monthName;
	}*/
	
	public static function getDays()
	{
		
		$monn = 'מרץ 2017';
		$allDays = \app\models\Schedual::find('employeeId')->where(['month' => $monn])->all();
		$result = ArrayHelper::getColumn($allDays, 'employeeId');
		
		return $result;		

	}
	
	 public function Send2($file)
    {
				
		$allemployees = \app\models\Employees::find()->all();
		$EmailsEmployees = ArrayHelper::getColumn($allemployees, 'email');		
				//$testo = ['0'=>'liadnj@gmail.com', '1'=>'liadn@npa.org.il'];
				

				
				for($i=0; $i<sizeof($EmailsEmployees); $i++){
								$emails = new Emails(); 
				//$employeesEmails = \app\models\Employees::getEmployeesEmailsToProjects($employees->id[$i]);
				$value = Yii::$app->mailer->compose()
								->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
								->setTo("$EmailsEmployees[$i]")
								->setSubject('שיבוץ')
								->setHtmlBody('<div style="text-align:right;"><h4>Done</h4></div>')
								->attach($file)
								->send();    
								$emails->save();
			}
	}
	
	

}
