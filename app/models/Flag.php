<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flag".
 *
 * @property integer $id
 * @property integer $eid
 * @property integer $month
 * @property integer $flag
 */
class Flag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eid', 'month', 'flag'], 'required'],
            [['eid', 'month', 'flag'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eid' => 'Eid',
            'month' => 'Month',
            'flag' => 'Flag',
        ];
    }
}
