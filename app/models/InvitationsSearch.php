<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Invitations;

/**
 * InvitationsSearch represents the model behind the search form about `app\models\Invitations`.
 */
class InvitationsSearch extends Invitations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'supplier_name', 'open_date',  'approval_status', 'order_status', 'notes'], 'safe'],
            [['quantity_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invitations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		$this->supplier_name == -1 ? $this->supplier_name = null : $this->supplier_name; 
		$this->item_name == -1 ? $this->item_name = null : $this->item_name; 
        // grid filtering conditions
		
        $query->andFilterWhere([
            'open_date' => $this->open_date,
            'quantity_order' => $this->quantity_order,
			'item_name' => $this->item_name,
			'supplier_name' => $this->supplier_name,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'supplier_name', $this->supplier_name])
         
            ->andFilterWhere(['like', 'approval_status', $this->approval_status])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
