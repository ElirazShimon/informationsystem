<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "item".
 *
 * @property string $item_name
 * @property integer $quantity_in_stock
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'quantity_in_stock','price_each'], 'required','message'=>'שדה חובה'],
            [['quantity_in_stock','price_each'], 'integer','message'=>'ספרות בלבד'],
            [['item_name'], 'string', 'max' => 200],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => 'שם פריט',
            'quantity_in_stock' => 'כמות במלאי',
			'price_each' => '  מחיר ליחידה  [₪]',
        ];
    }
	
	public static function getitems()
	{
		$allitems = self::find()->all();
		$allitemsArray = ArrayHelper::
					map($allitems, 'item_name', 'item_name');
		return $allitemsArray;						
	}
	
	public static function getItemWithAllItem()
	{
		$allItem = self::getitems();
		$allItem[-1] = 'כל הפריטים';
		$allItem = array_reverse ( $allItem, true );
		return $allItem;	
	}	
	
	public function getInvitations()
	{
		return $thus->hasMany(Invitations::className(),['item_name' =>'item_name']);
	}
	
	
	
	
	
	
	
	
}
