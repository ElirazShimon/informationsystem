<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Percent_of_jobs".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Employees $employees
 */
class PercentOfJobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'percent_of_jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

	
	public static function getPercentOfJobss()
	{
		$allPercentOfJobss = self::find()->all();
		$allPercentOfJobssArray = ArrayHelper::
					map($allPercentOfJobss, 'id', 'name');
		return $allPercentOfJobssArray;						
	}
	
	public static function getPercentOfJobssWithAllPercentOfJobss()
	{
		$allPercentOfJobss = self::getPercentOfJobss();
		$allPercentOfJobss[-1] = 'כל המצבים';
		$allPercentOfJobss = array_reverse ( $allPercentOfJobss, true );
		return $allPercentOfJobss;	
	}		
	
	
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employees::className(), ['Percent_of_jobs' => 'id']);
    }
}
