<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	
use yii\base\Model;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $define_project
 * @property string $team_leader
 * @property string $employee
 * @property string $location
 * @property string $due_date
 * @property string $notes
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['define_project', 'team_leader', 'employee'], 'required'],
            [['define_project', 'team_leader',  'location', 'notes','due_date'], 'string', 'max' => 255],
			 [['description_project'],'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'מספר פרויקט',
            'define_project' => 'שם הפרויקט',
			'description_project' =>'תיאור הפרויקט',
            'team_leader' => 'ראש צוות',
            'employee' => 'עובדים',
            'location' => 'מיקום',
			 //'start' => 'תאריך התחלה',
            'due_date' => 'תאריך יעד',
            'notes' => 'הערות',
        ];
    }
	
	public function getEmployeesssProject() // שייך לקשר עם טבלת עובדים לשדה של עובדים
    {
        return $this->hasOne(Employees::className(), ['id' => 'team_leader']);
    }
	
	public function getEmployeessProject() //שייך לקשר עם טבלת עובדים לשדה של ראש צוות
    {
        return $this->hasMany(Employees::className(), ['id' => 'team_leader']);
    }
	
	 public function getEmployeesnames(){
        $str = $this->employee;

        $arr = explode(", ", $str);
        $arrLength = count($arr);
        for($i=0;$i<=$arrLength-1; $i++){
            $id = intval($arr[$i]);
            $name = \app\models\Employees::findOne($id)->fullname;
            $arr[$i] = $name;
        }
        $names = implode(", ",$arr);
       return $names;

    }
	public function getEmployeesItem()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee']);
    }
}