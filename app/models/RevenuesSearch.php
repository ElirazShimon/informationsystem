<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Revenues;

/**
 * RevenuesSearch represents the model behind the search form about `app\models\Revenues`.
 */
class RevenuesSearch extends Revenues
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'day'], 'safe'],
            [['cash_desk_784', 'cash_desk_782', 'store'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Revenues::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date' => $this->date,
            'cash_desk_784' => $this->cash_desk_784,
            'cash_desk_782' => $this->cash_desk_782,
            'store' => $this->store,
        ]);

        $query->andFilterWhere(['like', 'day', $this->day]);

        return $dataProvider;
    }
}
