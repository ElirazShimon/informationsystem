<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\models\Flag;



	
/**
 * This is the model class for table "schedual".
 *
 * @property string $month
 * @property integer $employeesName
 * @property integer $days
 */
class Schedual extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedual';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'employeesName', 'days'], 'required'],
            [['employeesName', 'days'], 'string'],
			[['employeeId'],'integer'],
            [['month'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
	 
    public function attributeLabels()
    {
        return [
            'month' => 'חודש',
            'employeesName' => 'שם עובד',
            'days' => 'ימים לשיבוץ',
			'employeeId' => 'ת.ז עובד',
        ];
    }
	////////////////////////////////////////////////המקורי למעלה//////////////////////////////////////////////////////////////////////
	public static function getMonth($monthName)
	{
	//$month = $monthName;
	return $monthName;
	}
	
	
	
	public static function getDays($month,$day,$dt)
	{
		
		$allDays = Schedual::find('employeeId')->where(['month' => $month])->all();
		$resultId = ArrayHelper::getColumn($allDays, 'employeeId');
		
		$allEPDay = Schedual::find('days')->where(['employeeId' => $resultId,'month' => $month])->all();
		$resultDay = ArrayHelper::getColumn($allEPDay, 'days');
		
		//$strDay = ArrayHelper::getValue($resultDay , 'days');
		$employeesId = ArrayHelper::toArray($resultId);
		$employeesDays = ArrayHelper::toArray($resultDay);
		
		//$originalDate = "2017-03-17";
		//$newDate = date("dd/mm/Y", strtotime($originalDate));
		$newDate = date("d/m/Y", strtotime($day));

		$arr_length = count($employeesId);
		$employeesForDay = array();	
		$counter = 0;
		for($i=0;$i<$arr_length;$i++) {
				//$dayPerEmployee = Schedual::find('days')->where(['employeeId' => $employeesId[$i]])->one();
				//$arr_length3 = count($dayPerEmployee);
				$ExplodeDays=explode(',',$employeesDays[$i]);//converting to array...
				if (in_array($newDate,$ExplodeDays)){
				//$name = \app\models\Employees::findOne($employeesId[$i])->fullname;
				$inserted = array( "$employeesId[$i]" );
				array_splice( $employeesForDay, $counter, 0, $inserted );
				$counter++;
				}
			
			}
			/////////////////////////////////check percents///////////////////////////////////////
			//$CheckPercents = self::CheckPercents($employeesForDay,$month);
			$percent40Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>0])->all();
			$percent40Array = ArrayHelper::getColumn($percent40Sort, 'id');
			
			$percent70Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>2])->all();
			$percent70Array = ArrayHelper::getColumn($percent70Sort, 'id');
			
			$percent100Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>3])->all();
			$percent100Array = ArrayHelper::getColumn($percent100Sort, 'id');
			//$percent40Array = ArrayHelper::toArray($percent40Sort);
			
			for($k100 = 0; $k100 < sizeof($percent100Array);$k100++)
			{
			$weekendHouers100 = 0;
			
			
			$get100arr = \app\models\Events::find()->where(['employees'=>$percent100Array[$k100],'month'=>$month])->all();
			$a100Array = ArrayHelper::getColumn($get100arr, 'id');
			$Weekend100Array = ArrayHelper::getColumn($get100arr, 'created_date');
			
			$teken100 = \app\models\Schedual::Cal_Days_per_month($month,$dt)*1;
			$teken100Houers = round($teken100*8.5);
			$HouersToEmployee100 = sizeof($a100Array)*8.5;
			////// calculate weekends////////////////
			
			for($FRI100 = 0; $FRI100 < sizeof($Weekend100Array);$FRI100++){
			$myTimew100 = strtotime($Weekend100Array[$FRI100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Fri"){
			$HouersToEmployee100 = $HouersToEmployee100 -3.5 ;
			$weekendHouers100 = $weekendHouers100 + 5;
			}
			}
			
			for($SAT100 = 0; $SAT100 < sizeof($Weekend100Array);$SAT100++){
			$myTimew100 = strtotime($Weekend100Array[$SAT100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Sat"){
			$HouersToEmployee100 = $HouersToEmployee100 -10;
			$weekendHouers100 = $weekendHouers100 + 10;
			}
			}
			
			$myTimewALL100 = strtotime($day);
			$dayWALL100 = date("D",$myTimewALL100 ); // Sun - Sat
			if($dayWALL100 == "Fri"){
			if($weekendHouers100>25){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
					}
					
			if($dayWALL100 == "Sat"){
			if($weekendHouers100>20){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
					}
			
			if(round($HouersToEmployee100)>=$teken100Houers){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
			
			}
			/////////////////////////////////////////////////// 70% //////////////////////////////////////////////////
			for($k70 = 0; $k70 < sizeof($percent70Array);$k70++)
			{
			$weekendHouers70 = 0;
			
			$get70arr = \app\models\Events::find()->where(['employees'=>$percent70Array[$k70],'month'=>$month])->all();
			$a70Array = ArrayHelper::getColumn($get70arr, 'id');
			$Weekend70Array = ArrayHelper::getColumn($get70arr, 'created_date');
			
			$teken70 = \app\models\Schedual::Cal_Days_per_month($month,$dt)*0.7;
			$teken70Houers = round($teken70*8.5);
			$HouersToEmployee70 = sizeof($a70Array)*8.5;
			////// calculate weekends////////////////
			
			
			for($FRI70 = 0; $FRI70 < sizeof($Weekend70Array);$FRI70++){
			$myTimew70 = strtotime($Weekend70Array[$FRI70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Fri"){
			$HouersToEmployee70 = $HouersToEmployee70 -3.5 ;
			$weekendHouers70 = $weekendHouers70 + 5;
			}
			}
			
			for($SAT70 = 0; $SAT70 < sizeof($Weekend70Array);$SAT70++){
			$myTimew70 = strtotime($Weekend70Array[$SAT70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Sat"){
			$HouersToEmployee70 = $HouersToEmployee70 -8.5;
			$weekendHouers70 = $weekendHouers70 + 10;
			}
			}
			
			$myTimewALL70 = strtotime($day);
			$dayWALL70 = date("D",$myTimewALL70 ); // Sun - Sat
			if($dayWALL70 == "Fri"){
			if($weekendHouers70>25){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
					}
					
			if($dayWALL70 == "Sat"){
			if($weekendHouers70>20){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
					}
			
			
			if(round($HouersToEmployee70)>=$teken70Houers){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
			
			}
			//////////////////////////////////////////////END 70%
			
			/////////////////////////////////////////////////// 40% //////////////////////////////////////////////////
			for($k40 = 0; $k40 < sizeof($percent40Array);$k40++)
			{
			$weekendHouers40 = 0;
			
			
			$get40arr = \app\models\Events::find()->where(['employees'=>$percent40Array[$k40],'month'=>$month])->all();
			$a40Array = ArrayHelper::getColumn($get40arr, 'id');
			$Weekend40Array = ArrayHelper::getColumn($get40arr, 'created_date');
			
			$year1 = $dt->format('Y');
			$month1 = $dt->format('m');
			$teken40 = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*0.4);
			$teken40Houers = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*8.5*0.4);
			
			$HouersToEmployee40 = sizeof($a40Array)*8.5;
			
			
			for($SAT40 = 0; $SAT40 < sizeof($Weekend40Array);$SAT40++){
			$myTimew40 = strtotime($Weekend40Array[$SAT40]);
			$dayW40 = date("D",$myTimew40 ); // Sun - Sat
			if($dayW40 == "Sat"){
			$weekendHouers40 = $weekendHouers40 + 10;
			}
			}
			
			$myTimewALL40 = strtotime($day);
			$dayWALL40 = date("D",$myTimewALL40 ); // Sun - Sat
			
			if($dayWALL40 == "Sat"){
			if($weekendHouers40>=30){
			if(($key40 = array_search($percent40Array[$k40], $employeesForDay)) !== false) {
				unset($employeesForDay[$key40]);
					}
					}
					}
					
					if(\app\models\Schedual::last8month($percent40Array[$k40],$day)){
			if(round($HouersToEmployee40)>=$teken40Houers){
			if(($key40 = array_search($percent40Array[$k40], $employeesForDay)) !== false) {
				unset($employeesForDay[$key40]);
					}
					}
					}
			
			
			
			}
			//////////////////////////////////////////////END 40%
			
			
			////////////////////////////////////////CAL 6 DAYS //////////////////////////
			$qee = \app\models\Employees::find()->all();
			$qeeArray = ArrayHelper::getColumn($qee, 'id');
			
			for($SixDays = 0 ; $SixDays < sizeof($qeeArray); $SixDays++ ){
			$get6daysarr = \app\models\Events::find()->where(['employees'=>$qeeArray[$SixDays],'month'=>$month])->all();
			$LastDaysArray = ArrayHelper::getColumn($get6daysarr, 'created_date');
			$counter = 0;
			for($d6 = 1 ;$d6 <= 6 ;$d6++ ){
			$date = new \DateTime($day);
			$date->modify('-'.$d6.' day');
			$date->format('Y-m-d');
			$testr = $date->format('Y-m-d');
			for($d622 = 0 ;$d622 < sizeof($LastDaysArray) ;$d622++ ){
			if("$testr" == "$LastDaysArray[$d622]") {
				$counter ++;
				}
			}
			}
			if($counter == 6){
			if(($qsixday = array_search($qeeArray[$SixDays], $employeesForDay)) !== false) {
				unset($employeesForDay[$qsixday]);
					}			
				
				}
		}
			
			
			
			
			
			///////////////////////////////////////end 6 days//////////////////////////////////////
			
			$name = \app\models\Employees::getEmployeesPerDays($employeesForDay);
			return $name ;
		}
		
		
		
		public static function getDaysnoinsched($month,$day,$dt)
	{
		$allEmployees = Employees::find()->all();
		$EmployeesArray = ArrayHelper::getColumn($allEmployees, 'id');
		
		$employeesForDay = array();
		$employeesForDay = $EmployeesArray;	
		
		$allDays = Schedual::find('employeeId')->where(['month' => $month])->all();
		$resultId = ArrayHelper::getColumn($allDays, 'employeeId');
		
		$allEPDay = Schedual::find('days')->where(['employeeId' => $resultId,'month' => $month])->all();
		$resultDay = ArrayHelper::getColumn($allEPDay, 'days');
		
		//$strDay = ArrayHelper::getValue($resultDay , 'days');
		$employeesId = ArrayHelper::toArray($resultId);
		$employeesDays = ArrayHelper::toArray($resultDay);
		
		//$originalDate = "2017-03-17";
		//$newDate = date("dd/mm/Y", strtotime($originalDate));
		$newDate = date("d/m/Y", strtotime($day));

		$arr_length = count($employeesId);
		$employeesForDayQQ = array();	
		$counter = 0;
		for($i=0;$i<$arr_length;$i++) {
				//$dayPerEmployee = Schedual::find('days')->where(['employeeId' => $employeesId[$i]])->one();
				//$arr_length3 = count($dayPerEmployee);
				$ExplodeDays=explode(',',$employeesDays[$i]);//converting to array...
				if (in_array($newDate,$ExplodeDays)){
				//$name = \app\models\Employees::findOne($employeesId[$i])->fullname;
				$inserted = array( "$employeesId[$i]" );
				array_splice( $employeesForDayQQ, $counter, 0, $inserted );
				$counter++;
				}
			
			}
			
		
		for($initial = 0; $initial < sizeof($employeesForDayQQ);$initial++){
		if(($initialA = array_search($employeesForDayQQ[$initial], $employeesForDay)) !== false) {
				unset($employeesForDay[$initialA]);
					}
					}
		
		
			/////////////////////////////////check percents///////////////////////////////////////
			//$CheckPercents = self::CheckPercents($employeesForDay,$month);
			$percent40Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>0])->all();
			$percent40Array = ArrayHelper::getColumn($percent40Sort, 'id');
			
			$percent70Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>2])->all();
			$percent70Array = ArrayHelper::getColumn($percent70Sort, 'id');
			
			$percent100Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>3])->all();
			$percent100Array = ArrayHelper::getColumn($percent100Sort, 'id');
			//$percent40Array = ArrayHelper::toArray($percent40Sort);
			
			for($k100 = 0; $k100 < sizeof($percent100Array);$k100++)
			{
			$weekendHouers100 = 0;
			
			
			$get100arr = \app\models\Events::find()->where(['employees'=>$percent100Array[$k100],'month'=>$month])->all();
			$a100Array = ArrayHelper::getColumn($get100arr, 'id');
			$Weekend100Array = ArrayHelper::getColumn($get100arr, 'created_date');
			
			$teken100 = \app\models\Schedual::Cal_Days_per_month($month,$dt)*1;
			$teken100Houers = round($teken100*8.5);
			$HouersToEmployee100 = sizeof($a100Array)*8.5;
			////// calculate weekends////////////////
			
			for($FRI100 = 0; $FRI100 < sizeof($Weekend100Array);$FRI100++){
			$myTimew100 = strtotime($Weekend100Array[$FRI100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Fri"){
			$HouersToEmployee100 = $HouersToEmployee100 -3.5 ;
			$weekendHouers100 = $weekendHouers100 + 5;
			}
			}
			
			for($SAT100 = 0; $SAT100 < sizeof($Weekend100Array);$SAT100++){
			$myTimew100 = strtotime($Weekend100Array[$SAT100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Sat"){
			$HouersToEmployee100 = $HouersToEmployee100 -10;
			$weekendHouers100 = $weekendHouers100 + 10;
			}
			}
			
			$myTimewALL100 = strtotime($day);
			$dayWALL100 = date("D",$myTimewALL100 ); // Sun - Sat
			if($dayWALL100 == "Fri"){
			if($weekendHouers100>25){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
					}
					
			if($dayWALL100 == "Sat"){
			if($weekendHouers100>20){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
					}
			
			if(round($HouersToEmployee100)>=$teken100Houers){
			if(($key100 = array_search($percent100Array[$k100], $employeesForDay)) !== false) {
				unset($employeesForDay[$key100]);
					}
					}
			
			}
			/////////////////////////////////////////////////// 70% //////////////////////////////////////////////////
			for($k70 = 0; $k70 < sizeof($percent70Array);$k70++)
			{
			$weekendHouers70 = 0;
			
			$get70arr = \app\models\Events::find()->where(['employees'=>$percent70Array[$k70],'month'=>$month])->all();
			$a70Array = ArrayHelper::getColumn($get70arr, 'id');
			$Weekend70Array = ArrayHelper::getColumn($get70arr, 'created_date');
			
			$teken70 = \app\models\Schedual::Cal_Days_per_month($month,$dt)*0.7;
			$teken70Houers = round($teken70*8.5);
			$HouersToEmployee70 = sizeof($a70Array)*8.5;
			////// calculate weekends////////////////
			
			
			for($FRI70 = 0; $FRI70 < sizeof($Weekend70Array);$FRI70++){
			$myTimew70 = strtotime($Weekend70Array[$FRI70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Fri"){
			$HouersToEmployee70 = $HouersToEmployee70 -3.5 ;
			$weekendHouers70 = $weekendHouers70 + 5;
			}
			}
			
			for($SAT70 = 0; $SAT70 < sizeof($Weekend70Array);$SAT70++){
			$myTimew70 = strtotime($Weekend70Array[$SAT70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Sat"){
			$HouersToEmployee70 = $HouersToEmployee70 -8.5;
			$weekendHouers70 = $weekendHouers70 + 10;
			}
			}
			
			$myTimewALL70 = strtotime($day);
			$dayWALL70 = date("D",$myTimewALL70 ); // Sun - Sat
			if($dayWALL70 == "Fri"){
			if($weekendHouers70>25){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
					}
					
			if($dayWALL70 == "Sat"){
			if($weekendHouers70>20){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
					}
			
			
			if(round($HouersToEmployee70)>=$teken70Houers){
			if(($key70 = array_search($percent70Array[$k70], $employeesForDay)) !== false) {
				unset($employeesForDay[$key70]);
					}
					}
			
			}
			//////////////////////////////////////////////END 70%
			
			/////////////////////////////////////////////////// 40% //////////////////////////////////////////////////
			for($k40 = 0; $k40 < sizeof($percent40Array);$k40++)
			{
			$weekendHouers40 = 0;
			
			
			$get40arr = \app\models\Events::find()->where(['employees'=>$percent40Array[$k40],'month'=>$month])->all();
			$a40Array = ArrayHelper::getColumn($get40arr, 'id');
			$Weekend40Array = ArrayHelper::getColumn($get40arr, 'created_date');
			
			$year1 = $dt->format('Y');
			$month1 = $dt->format('m');
			$teken40 = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*0.4);
			$teken40Houers = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*8.5*0.4);
			
			$HouersToEmployee40 = sizeof($a40Array)*8.5;
			
			////// calculate weekends////////////////
			for($SAT40 = 0; $SAT40 < sizeof($Weekend40Array);$SAT40++){
			$myTimew40 = strtotime($Weekend40Array[$SAT40]);
			$dayW40 = date("D",$myTimew40 ); // Sun - Sat
			if($dayW40 == "Sat"){
			$weekendHouers40 = $weekendHouers40 + 10;
			}
			}
			
			$myTimewALL40 = strtotime($day);
			$dayWALL40 = date("D",$myTimewALL40 ); // Sun - Sat
			
			if($dayWALL40 == "Sat"){
			if($weekendHouers40>=30){
			if(($key40 = array_search($percent40Array[$k40], $employeesForDay)) !== false) {
				unset($employeesForDay[$key40]);
					}
					}
			}
			
			if(\app\models\Schedual::last8month($percent40Array[$k40],$day)){
			if(round($HouersToEmployee40)>=$teken40Houers || ($teken40Houers-round($HouersToEmployee40))<8.5){
			if(($key40 = array_search($percent40Array[$k40], $employeesForDay)) !== false) {
				unset($employeesForDay[$key40]);
					}
					}
					}
			
			}
			//////////////////////////////////////////////END 40%
			
			
			////////////////////////////////////////CAL 6 DAYS //////////////////////////
			$qee = \app\models\Employees::find()->all();
			$qeeArray = ArrayHelper::getColumn($qee, 'id');
			
			for($SixDays = 0 ; $SixDays < sizeof($qeeArray); $SixDays++ ){
			$get6daysarr = \app\models\Events::find()->where(['employees'=>$qeeArray[$SixDays],'month'=>$month])->all();
			$LastDaysArray = ArrayHelper::getColumn($get6daysarr, 'created_date');
			$counter = 0;
			for($d6 = 1 ;$d6 <= 6 ;$d6++ ){
			$date = new \DateTime($day);
			$date->modify('-'.$d6.' day');
			$date->format('Y-m-d');
			$testr = $date->format('Y-m-d');
			for($d622 = 0 ;$d622 < sizeof($LastDaysArray) ;$d622++ ){
			if("$testr" == "$LastDaysArray[$d622]") {
				$counter ++;
				}
			}
			}
			if($counter == 6){
			if(($qsixday = array_search($qeeArray[$SixDays], $employeesForDay)) !== false) {
				unset($employeesForDay[$qsixday]);
					}			
				
				}
		}
			
			///////////////////////////////////////end 6 days//////////////////////////////////////
			
			
			$name = \app\models\Employees::getEmployeesPerDays($employeesForDay);
			return $name ;
		}

		
		
		
		public static function last8month($employeeId,$dt){		
			
			$res = false;
			$counter = 0 ;
				for($k40 = 1; $k40 <= 8;$k40++)
			{
			
			$newMonoth = "";
			$date = new \DateTime($dt);
			$date->modify('-'.$k40.' month');
			$date->format('Y M');
			 $testr = $date->format('Y');
			
			
if($date->format('m') == "01")
$newMonoth = "ינואר $testr";
if($date->format('m') == "02")
$newMonoth = "פברואר $testr";
if($date->format('m') == "03")
$newMonoth = "מרץ $testr";
if($date->format('m') == "04")
$newMonoth = "אפריל $testr";
if($date->format('m') == "05")
$newMonoth = "מאי $testr";
if($date->format('m') == "06")
$newMonoth = "יוני $testr";
if($date->format('m') == "07")
$newMonoth = "יולי $testr";
if($date->format('m') == "08")
$newMonoth = "אוגוסט $testr";
if($date->format('m') == "09")
$newMonoth = "ספטמבר $testr";
if($date->format('m') == "10")
$newMonoth = "אוקטובר $testr";
if($date->format('m') == "11")
$newMonoth = "נובמבר $testr";
if($date->format('m') == "12")
$newMonoth = "דצמבר $testr";

$get40arr = \app\models\Events::find()->where(['employees'=>$employeeId,'month'=>$newMonoth])->all();
$a40Array = ArrayHelper::getColumn($get40arr, 'id');
 
 $teken40Houers = round(cal_days_in_month(CAL_GREGORIAN, $date->format('m'),$date->format('Y'))*8.5*0.4);
// count($a40Array)*8.5;
if(count($a40Array)*8.5>$teken40Houers)
$counter++;
}
if($counter == 8)
$res = true;
return $res;
}	
		public static function Cal_Days_per_month($month,$dt){
		/////////////////////////////Cal Days per month ////////////////////////////
			$timestamp = strtotime("$month");
	
			//$dt = new DateTime("$timestamp");
			//$dt = strtotime("$month");
			$year = $dt->format('Y');
			$month = $dt->format('m');
		
			//$myTime = strtotime('0/'.$month.'/'.$year);  // Use whatever date format you want
			$myTime = strtotime(''.$month.'/01/'.$year);
			$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31
			$workDays = 0;
			while($daysInMonth > 0)
				{
				$day = date("D", $myTime); // Sun - Sat
				if($day != "Fri" && $day != "Sat")
				$workDays++;
	
					$daysInMonth--;
					$myTime += 86400; // 86,400 seconds = 24 hrs.
				}
			
			return $workDays;
			}
			
			
			
			
			
			

			
			public static function Cal_Weekend_per_month($month,$dt){
		/////////////////////////////Cal Weekend per month ////////////////////////////
			$timestamp = strtotime("$month");
			$year = $dt->format('Y');
			$month = $dt->format('m');
			$myTime = strtotime(''.$month.'/01/'.$year);
			$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31
			$workDays = 0;
			while($daysInMonth > 0)
				{
				$day = date("D", $myTime); // Sun - Sat
				if($day != "Sun" && $day != "Mon" && $day != "Tue" && $day != "Wed" && $day != "Thu" )
				$workDays++;
	
					$daysInMonth--;
					$myTime += 86400; // 86,400 seconds = 24 hrs.
				}
			
			return $workDays;
			}
			

				
	
	}
	

