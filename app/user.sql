-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2016 at 10:54 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `backup_webnicoi_prat`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(0, 'ff', '$2y$13$22unOZhSodytfHs4bGd7heG.YuWRpvzbgsbgDDIBYsQNpTUtVaMgS', 'EauMfRzdSbTGlyiCHUjJHaJuOjtUQFKp', 1477905157, 1477905157, 255, 255),
(254, 'teamleader', '$2y$13$3Pvg9yLywuxbKfqcXTKecOgmaIcRGHle66xQm782bXZnxzknGa5S6', 'WnVA1xkwvdC5AT069FcKCwDE2r9UPlQa', 1477560874, 1477647177, NULL, 255),
(255, 'admin', '$2y$13$CSBZcZvsV0f4DATidh4Cvuw8t2a8W02/RsvXB/LoyOASkrstoxxjS', '7O_FUKnntTMBo-jXxks_H9rCcYr9Ta5j', 1477570055, 1477647122, NULL, 255),
(300972974, 'employee', '$2y$13$wMfE3u3n0mcx8Xtnrn3VHedM5/Qhi0k2AEH5y/CoOUFXeL6xKTMUy', 'pehIu5RJlsjjI2alP4HdCPNZX08TB4pU', 1477644374, 1477644374, 255, 255);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
