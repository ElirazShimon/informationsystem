<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buroc */

$this->title = 'עדכון נושא: ' . $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'מטלות משרד', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subject, 'url' => ['view', 'id' => $model->subject]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="buroc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
