<?php 
use kartik\widgets\FileInput;
 use yii\helpers\Url;
 use yii\helpers\Html;

echo '<h2 align = "center">מסמכים '."$model->fullname".'</h2>'
?>
<?= Html::a('חזור לפרטי עובד', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<hr>
<?php
echo '<label class="control-label">הוסף מסמכים</label>';
echo FileInput::widget([
    'model' => $model,
    'name' => 'ImageManager[attachment]',
    'options' => ['multiple' => true,'overwriteInitial '=>false],
	 'pluginOptions' => [
		
		'uploadUrl' => \yii\helpers\Url::to(['/employees/upload-files']),
		'browseIcon' => '<i style = "margin-left:5%;" class="glyphicon glyphicon-paperclip"></i> ',
        'browseLabel' =>  'בחר קבצים',
		'removeLabel' =>  'מחק קבצים',
		'uploadLabel' => 'העלה קבצים',
		'uploadExtraData' => [
            'ImageManager[item_id]' => $model->id  
        ],
		'initialPreview'=>[
            "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg",
            "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg"
        ],
        'initialPreviewAsData'=>true,
        'initialCaption'=>"The Moon and the Earth",
        'initialPreviewConfig' => [
            ['caption' => 'Moon.jpg', 'size' => '873727'],
            ['caption' => 'Earth.jpg', 'size' => '1287883'],
        ],
        'overwriteInitial'=>false,
        'maxFileCount' => 30
	],
]);
 

//echo "$model->fullname"
?>

