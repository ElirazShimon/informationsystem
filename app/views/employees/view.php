<script>
      function printContent(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
     }
   </script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\web\JsExpression;
use yii\helpers\Url;
use metalguardian\Fotorama;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\spinner\Spinner;
/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'עובדים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

 
$this->registerJs("$(function() {
   $('#popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});");


    yii\bootstrap\Modal::begin([
	    'header' => '<h2>תמונת עובד</h2>',
		'id' =>'modal',
	
		]);
		
		echo '<button style="margin-left:1%" onclick="printContent("imgemployee");" class = "btn btn-success active">הורד תמונה</button>';
		echo '<button class = "btn btn-default active">הדפס תמונה</button>';
		echo '<hr>';
		echo '<div align="center">';
		echo '<img id="imgemployee" src = "http://localhost/app/web/Employees_images'.'/' .$model->image.'" >';
		echo '</div>';
		
    yii\bootstrap\Modal::end();
	
	

?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		

		
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?= Html::a('מסמכי עובד', ['files', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>
	
	
	
<!--<img style= "weight:100px; height:100px;" src="web/uploads/רם.jpg "></img>-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
			'color',
           
			
			[ // the role name 
				'label' => $model->attributeLabels()['role'],
				'value' => $model->roleItem->name,	
			],			
			
          
			 //'Percent_of_jobs',
			[ // the Percent_of_jobs name 
				'label' => $model->attributeLabels()['Percent_of_jobs'],
				'value' => $model->percent_of_jobsItem->name,	
			],	
			[ // the armed name 
				'label' => $model->attributeLabels()['armed'],
				'value' => $model->armedItem->name,	
			],	
			'email',
            'cellphone',
            'adress',
           			
			/*[
				'attribute' => 'color',
				'value' => 'colorInput::classname()',
				//'format' => ['color', ['width' => '100', 'height'=> '100']],
				
			],*/
			
				
			[
				'attribute' => 'image',
				
				//'value' => Html:: a('http://localhost/project_04.05.17/masterproject/web/uploads'.'/' .$model->image,['http://localhost/project_04.05.17/masterproject/web/uploads'.'/' .$model->image]),
				'value' => 'http://localhost/app/web/Employees_images'.'/' .$model->image,
				'format' => ['image', ['width' => '100','onClick' => 'function myFunction()','id' => 'popupModal', 'class' => 'btn', 'height'=> '100']]
			
			],
		
        ],
    ]) 
	
	
?>
	
<div align = "center;">
<h2>מסמכי עובד:</h2>
<?php
   
		$fotorama = \metalguardian\fotorama\Fotorama::begin(
        [
            'options' => [
                'loop' => true,
                'hash' => true,
                'ratio' => 800/600,
            ],
            'spinner' => [
                'lines' => 20,
            ],
            'tagName' => 'span',
            'useHtmlData' => false,
            'htmlOptions' => [
                'class' => 'custom-class',
                'id' => 'custom-id',
            ],
        ]
    );
	 $TestModel = \app\models\ImageManager::find()->all();
     foreach($TestModel as $one)
	 //for($r = 0; $r < sizeof($model->files); $r++)
	{
		echo Html::img('@web/Employees_files/'.$one->name);
		//echo Html::img('/Employees_files/'.$one->name);
	}
   $fotorama->end();
     ?>
	 
		
</div>
</div>
