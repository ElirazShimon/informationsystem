 <script>
      function printContent(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
     }
   </script>
   <script>
      function fullScreen(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.download();
         document.body.innerHTML = restorepage;
		 
     }
   </script>
   
  <body> 
  
   
  

<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\spinner\Spinner;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\mpdf\Pdf;
use yii\imagine\Image;




/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'שיבוץ';
//$this->params['breadcrumbs'][] = $this->title;



 ?>
 
 
 	<div align="center">
					<!--<h1 align="center"  class="glyphicon glyphicon-download" style="cursor:pointer;" title="Click to Download Page as PDF"></h1>-->
				</div>
				<br>
					<br>
				<section id="page_content">
				
				
				
				
				
		
			
 


<!--<h2 style = "color:#ddd12; text-align:center;">שיבוץ</h2>-->
<?php 

$JSEventClick = <<<EOF
function(calEvent, jsEvent, view) {
    //alert('Event: ' + calEvent.title);
    //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
    //alert('ID: ' + $(this).id);
	var date = $(this).attr('data-date');
	id= calEvent.id;
	  // alert('View: ' + id);

	// $(this).remove();
    // change the border color just for fun
			
			
	
	 document.location.href='/app/web/events/view?id=' +calEvent.id;
	 
				
	
		
   
}
EOF;
	\yii2assets\fullscreenmodal\FullscreenModal::begin([
   'header' => '<h4 class="modal-title text-center">שיבוץ מסך מלא</h4>',
   //'header' => '<h4 class="modal-title text-center">שיבוץ מסך מלא</h4>',
  // 'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           //    <button type="button" class="btn btn-primary">Save changes</button>',
		  'toggleButton' => ['label' => 'מסך מלא','class'=>'btn btn-danger'],
]);
?>


 <?php \yii2assets\fullscreenmodal\FullscreenModal::end();?>
 <!--   < ?= Html::a('שלח שיבוץ', ['fullscreen'], ['class' => 'btn btn-info active glyphicon glyphicon-send','margin-right'=>'2%']) ?>
<!-- <button class="btn btn-info active" style="margin-right:2%"; onclick="fullScreen('div1')">מסך מלא</button>-->
 <button style="margin-right:2%;" class="btn btn-primary active" onclick="printContent('div1')">הדפס שיבוץ</button>
 <button style="margin-right:2%;" id="qxzx" class="btn btn-warning active">הורד שיבוץ</button>
 <button style="margin-right:2%;"  id="saveme1" class="btn btn-success active"> <i style="margin-left:8%;" class = "glyphicon glyphicon-send"></i>שלח שיבוץ</button>

 
 <?php    
if (isset($_POST['imgBase64'])){
define('UPLOAD_DIR', 'shiboozuploads/');
$img = $_POST['imgBase64'];
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
$file = UPLOAD_DIR . uniqid() . '.png';
$success = file_put_contents($file, $data);
$name = \app\models\Events::Send2($file);
print $success ? $file : 'Unable to save the file.';
}

?> 
				
				
<hr>



	
  <!--  <h1>< ?= Html::encode($this->title) ?></h1>-->

<script>
function myFunction() {
			var div = document.getElementsByClassName("fc-center")[0];
			var monthName = div.getElementsByTagName("h2")[0].innerHTML;
			var x = document.getElementsByClassName("example");
    x[0].innerHTML = monthName;

	}
</script>
	
	
	<?php
	
	//$workDays = \app\models\Schedual::Cal_Days_per_month($currentMonth,$dt);
	
	echo $currentMonth = '<div class="example"></div>'; 
	//function CalDays(){
	//$currentMonth = '<div class="example"></div>'; 
	//echo $currentMonth;
	//echo $newCorrent = $currentMonth;
	$timestamp = strtotime("$currentMonth");
	
	$dt = new DateTime("$timestamp");
	 $year = $dt->format('Y');
	$month = $dt->format('m');
		
	$myTime = strtotime(''.$month.'/01/'.$year);  // Use whatever date format you want
	$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31
	$workDays = 0;
	

	while($daysInMonth > 0)
		{
		$day = date("D", $myTime); // Sun - Sat
		if($day != "Fri" && $day != "Sat")
        $workDays++;
	
    $daysInMonth--;
    $myTime += 86400; // 86,400 seconds = 24 hrs.
	
	
}
	?>
	
	


<?php $this->registerJsFile('app/web/main33.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 
<?php $this->registerJsFile('app/web/jspdf.debug.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 

<div class="loader" style="display:none;  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px; //Half of the height of the loading graphic
  margin-left: -50px; //Half of the width of the loading graphic"></div>

<div id="loading-indicator" style="display:none;  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px; //Half of the height of the loading graphic
  margin-left: -50px; //Half of the width of the loading graphic">
<!--img src="http://dkclasses.com/images/loading.gif" id="loading-indicator" style="display:none" />-->


<?= Spinner::widget([
    'preset' => Spinner::LARGE,
    'color' => 'blue',
    'align' => 'center',
	
	
])?>
</div>
    <?php
	
        Modal::begin([
                'header'=>'<h4>שיבוץ</h4>',
				'id' => 'modal',
                'size'=>'modal-lg',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='modalContent'></div>";
     
        Modal::end();
    ?>
	
	    <?php
	
        Modal::begin([
                'header'=>'<h4>מסך שליחה</h4>',
				'id' => 'mimage',
                'size'=>'modal-sm',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='savemimage'></div>";
		?>
		<hr>
		<button type="button" class="btn btn-default active" data-dismiss="modal">סגור</button>
     <?php
        Modal::end();
    ?>
	
	
	
	
	<?php
	$JSCode = <<<EOF
function(start, end) {
    var title = prompt('Event Title:');
    var eventData;
    if (title) {
        eventData = {
            title: title,
            start: start,
            end: end
        };
        $('#w0').fullCalendar('renderEvent', eventData, true);
    }
    $('#w0').fullCalendar('unselect');
}
EOF;
$JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;
/*$JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;



$JSCode = <<<EOF
function(start, end) {
   // var title = prompt('Event Title:');
    var eventData;
    if (title) {
        eventData = {
            title: title,
			id: id,
			created_date: created_date,
            start: start,
            end: end
        };
       // $('#w0').fullCalendar('renderEvent', eventData, true);
    }
   // $('#w0').fullCalendar('unselect');
}
EOF;*/



?>
<span id="widget" class="widget" field="AGE" roundby="20" description="Patient age, in years">
  <div id="div1" class= "as1">
  <section class = "as" id="canvas">
    
<?= \yii2fullcalendar\yii2fullcalendar::widget(array(
		 'clientOptions' => [
		 'eventClick' => new JsExpression($JSEventClick),
		'drop' => new JsExpression($JSDropEvent),
		  ],
      'events'=> $events,
	        'options' => [
        'lang' => 'he',
		'dayNames' => true,
		
		
					],
  ));
?>

</section>
</div>

<div id="div2" class= "as2" style = "width:100%;height:100%;">
  <section class = "as2" id="canvas2">
    
<?php \yii2fullcalendar\yii2fullcalendar::widget(array(
		 'clientOptions' => [
		 'eventClick' => new JsExpression($JSEventClick),
		 'selectable' => true,
                    'selectHelper' => true,
                    'droppable' => true,
                    'editable' => true,
                    'drop' => new JsExpression($JSDropEvent),
                   'select' => new JsExpression($JSCode),
                    'eventClick' => new JsExpression($JSEventClick),
		  ],
					
      'events'=> $events,
	        'options' => [
        'lang' => 'he',
		'dayNames' => true,
		

		
		
					],
  ));
?>

</section>
</div>
</span>
<div id="editor"></div>
	</div>	
	

		
   </div>

		 </body>