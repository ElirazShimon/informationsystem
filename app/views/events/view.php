<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
$GetExplodeNames = explode(',', $model->id); 
		 $name = \app\models\Employees::findOne($GetExplodeNames[1])->fullname;
		$this->title = "$name ,  $GetExplodeNames[0]";

$this->params['breadcrumbs'][] = ['label' => 'שיבוץ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view">

		
    <h1><?= Html::encode("$name ,  $GetExplodeNames[0]") ?></h1>

    <p>
     <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'אתה רוצה להסיר את העובד מהשיבוץ?',
                'method' => 'post',
            ],
        ]) ?>
		 <?= Html::a('חזרה לשיבוץ', ['index'], ['class' => 'btn ', 'style'=>'font-weight:bold']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
        
			[
		  'attribute' => 'employee',
		  'label'=>'עובדים',
		  'format' => 'raw',
		  'value' => \app\models\Employees::findOne($GetExplodeNames[1])->fullname,
		  
		  ],
            'projects',
            'holyday',
            'description',
        ],
    ]) ?>

</div>


