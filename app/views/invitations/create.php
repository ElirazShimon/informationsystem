<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Invitations */

$this->title = 'יצירת הזמנה';
$this->params['breadcrumbs'][] = ['label' => 'הזמנות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invitations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
