<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messages-form">

    <?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'author')->textInput(['readonly' => true,'value' =>Yii::$app->user->identity->username]) ?>
	
		<?= $form->field($model, 'title')->textInput(['placeholder' => 'הקלד נושא'],['maxlength' => true]) ?>
			
		<?= $form->field($model, 'description')->textArea( ['rows' => 8, 'placeholder' => 'הקלד הודעה']) ?>
			 
   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
