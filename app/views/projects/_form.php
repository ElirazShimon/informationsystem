<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use kartik\select2\Select2;


use dosamigos\datepicker\DatePicker;
use dosamigos\multiselect\MultiSelect;

use app\models\Employees;
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    
	
	<?= $form->field($model, 'define_project')->textarea(['maxlength' => true]); ?>


     <?=  $form->field($model, 'team_leader')->widget(Select2::classname(), [
   
    'data' => ['ראש צוות לפרויקט'=> Employees::getEmployees()],
	'language' => 'he',
	'options' => ['required'=> true,
	'dir' => 'rtl',
	
	
	 ],
    'pluginOptions' => [
	'dir' => 'rtl',
	'allowClear' => true,
	'placeholder' => ' בחר ראש צוות לפרויקט',
    'tags' => true,
	
    ],
]);
?>
	
	<? /*= $form->field($model, 'employee')->widget(MultiSelect::classname(), [
      'data' => [ (Employees::getEmployees())],
    //"options" => ['multiple'=>"multiple"], // for the actual multiselect
    'options' => ['multiple'=>"multiple"],
    "clientOptions" => 
        [
            "includeSelectAllOption" => true,
           
        ], 
]);
	*/?>
	<?=  $form->field($model, 'employee')->widget(Select2::classname(), [
   
    'data' => ['בחר עובדים'=> Employees::getEmployees()],
	'language' => 'he',
	'options' => ['required'=> true,
	'dir' => 'rtl',
	
	'multiple' => true,
	 ],
    'pluginOptions' => [
	'dir' => 'rtl',
	'allowClear' => true,
	'placeholder' => ' בחר עובדים',
    'tags' => true,
	
    ],
]);
?>

	
	
	
    
	<?= $form->field($model, 'due_date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
		
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
		
        'format' => 'dd/mm/yyyy',
        ]
]);?>
   

	<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
	
	

	

    <div class="form-group">
	בעת לחיצה על כפתור שלח ישלח מייל לעובדים הנ"ל עם פרטי הפרויקט,
	תופיע הודעת אישור שליחת המייל,
	בהצלחה!
	<hr>
        <?= Html::submitButton($model->isNewRecord ? 'שלח' : 'עדכן', ['id'=>'sendme','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php $this->registerJsFile('app/web/main33.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 

<?php
	
        Modal::begin([
                'header'=>'<h4>מסך שליחה</h4>',
				'id' => 'ssemage',
                'size'=>'modal-sm',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='sendmeimage'></div>";
		?>
		<hr>
		<button type="button" class="btn btn-default active" data-dismiss="modal">סגור</button>
     <?php
        Modal::end();
    ?>
	
    <?php ActiveForm::end(); ?>

</div>