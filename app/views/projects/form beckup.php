<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;

use dosamigos\datepicker\DatePicker;
use dosamigos\multiselect\MultiSelect;


use app\models\Employees;
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

<?= MultiSelect::widget([
    'data' => ['super', 'natural'],
    'name' => 'Test'
]) ?>

<?php echo MultiSelect::widget([
    'id'=>"multiXX",
    "options" => ['multiple'=>"multiple"], // for the actual multiselect
    'data' => [ (Employees::getEmployees())], // data as array
    'name' => 'employee', // name for the form
    "clientOptions" => 
        [
            "includeSelectAllOption" => true,
            'numberDisplayed' => 2
        ], 
]); ?>
    <?php $form = ActiveForm::begin(); ?>

    
	
	<?= $form->field($model, 'define_project')->textarea(['maxlength' => true]); ?>




  

    <?= $form->field($model, 'team_leader')->dropdownList((Employees::getEmployees())) ; ?>
	
	<?= $form->field($model, 'employee')->widget(MultiSelect::classname(), [
      'data' => [ (Employees::getEmployees())],
    //"options" => ['multiple'=>"multiple"], // for the actual multiselect
    'options' => ['multiple'=>"multiple",'placeholder' => 'Select a employee ...'],
    "clientOptions" => 
        [
            "includeSelectAllOption" => true,
           
        ], 
]);
	?>

	<?= 
  $form->field($model, 'employee')            
         ->dropDownList(Employees::getEmployees(),
         ['multiple'=>'multiple']             
        )->label("בחר עובדים");
 ?>

    
	<?= $form->field($model, 'due_date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
		
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
		
        'format' => 'dd/mm/yyyy',
        ]
]);?>
   

	<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
	
	

	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'שלח' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
