<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = 'עדכון פרויקט: ' . $model->define_project;
$this->params['breadcrumbs'][] = ['label' => 'פרויקטים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->define_project, 'url' => ['view', 'define_project' => $model->define_project]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="projects-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
