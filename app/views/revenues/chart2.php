<?php

use miloschuman\highcharts\Highcharts;
?>




<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart2"></div>


<?php


$sql = "SELECT  YEAR(date) as years , SUM(DISTINCT `cash_desk_784`)+ SUM(DISTINCT `cash_desk_782`)+ SUM(DISTINCT `store`) as total
FROM revenues
GROUP BY YEAR(date)";
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	'name'=>$data['years'],
	'y' => $data['total'] *1,
	'drilldown' => $data ['total']
	];	
}


$main = json_encode($main_data);

$sql = "select year(g.date) as ghh
,SUM(DISTINCT g.`cash_desk_784`)+ SUM(DISTINCT g.`cash_desk_782`)+ SUM(DISTINCT g.`store`) as total
,SUM(DISTINCT g1.`cash_desk_784`)+ SUM(DISTINCT g1.`cash_desk_782`)+ SUM(DISTINCT g1.`store`) as ya
, SUM(DISTINCT g2.`cash_desk_784`)+ SUM(DISTINCT g2.`cash_desk_782`)+ SUM(DISTINCT g2.`store`)  as fv
,SUM(DISTINCT g3.`cash_desk_784`)+ SUM(DISTINCT g3.`cash_desk_782`)+ SUM(DISTINCT g3.`store`) as mr
,SUM(DISTINCT g4.`cash_desk_784`)+ SUM(DISTINCT g4.`cash_desk_782`)+ SUM(DISTINCT g4.`store`) as ap
,SUM(DISTINCT g5.`cash_desk_784`)+ SUM(DISTINCT g5.`cash_desk_782`)+ SUM(DISTINCT g5.`store`) as ma
,SUM(DISTINCT g6.`cash_desk_784`)+ SUM(DISTINCT g6.`cash_desk_782`)+ SUM(DISTINCT g6.`store`) as yo
,SUM(DISTINCT g7.`cash_desk_784`)+ SUM(DISTINCT g7.`cash_desk_782`)+ SUM(DISTINCT g7.`store`) as yol
,SUM(DISTINCT g8.`cash_desk_784`)+ SUM(DISTINCT g8.`cash_desk_782`)+ SUM(DISTINCT g8.`store`) as ag
,SUM(DISTINCT g9.`cash_desk_784`)+ SUM(DISTINCT g9.`cash_desk_782`)+ SUM(DISTINCT g9.`store`) as sp
,SUM(DISTINCT g10.`cash_desk_784`)+ SUM(DISTINCT g10.`cash_desk_782`)+ SUM(DISTINCT g10.`store`) oc
,SUM(DISTINCT g11.`cash_desk_784`)+ SUM(DISTINCT g11.`cash_desk_782`)+ SUM(DISTINCT g11.`store`) as nv
,SUM(DISTINCT g12.`cash_desk_784`)+ SUM(DISTINCT g12.`cash_desk_782`)+ SUM(DISTINCT g12.`store`) as de

from revenues g
left join revenues g1 on g.date = g1.date and month(g.date) = 1
left join revenues g2 on g.date = g2.date and month(g.date) = 2
left join revenues g3 on g.date = g3.date and month(g.date) = 3
left join revenues g4 on g.date = g4.date and month(g.date) = 4
left join revenues g5 on g.date = g5.date and month(g.date) = 5
left join revenues g6 on g.date = g6.date and month(g.date) = 6
left join revenues g7 on g.date = g7.date and month(g.date) = 7
left join revenues g8 on g.date = g8.date and month(g.date) = 8
left join revenues g9 on g.date = g9.date and month(g.date) = 9
left join revenues g10 on g.date = g10.date and month(g.date) = 10
left join revenues g11 on g.date = g11.date and month(g.date) = 11
left join revenues g12 on g.date = g12.date and month(g.date) = 12

group by year(g.date)";

$rawData = yii::$app->db->createCommand($sql)->queryAll();
$sub_data =[];
foreach ($rawData as $data){
	$sub_data[] =[
	'id' => $data['total'],
	'name' => $data['ghh'] *1,
	'data' => [['ינואר',$data['ya']*1],['פברואר',$data['fv']*1],['מרס',$data['mr']*1],['אפריל',$data['ap']*1],['מאי',$data['ma']*1],['יוני',$data['yo']*1],['יולי',$data['yol']*1],['אוגוסט',$data['ag']*1],['ספטמבר',$data['sp']*1],['אוקטובר',$data['oc']*1],['נובמבר',$data['nv']*1],['דצמבר',$data['de']*1]]
	];	
}


$sub = json_encode($sub_data);




?>




<?php	
	
	 $this->registerJs("$(function () {
    // Create the chart
    $('#chart2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'הכנסות לאורך השנים: 2012-2017'
        },
      
        xAxis: {
            type: 'category',
			        useHTML: Highcharts.hasBidiBug,
					useHTML:true

			
        },
        yAxis: {
            title: {
                text: ' סך הכנסות  [₪]',
				  useHTML: Highcharts.hasBidiBug,
				  useHTML:true
				  },
				  


        },
		labels: {
				useHTML: Highcharts.hasBidiBug,
				useHTML:true
				},
        legend: {
            enabled: true,
			useHTML:true
        },
		tooltip: {
        useHTML: true
    },
        plotOptions: {
            series: {
				
                borderWidth: 0,
				useHTML:true,

			
				
                dataLabels: {
                    enabled: true,
					useHTML: Highcharts.hasBidiBug,
					            useHTML:true,

                    format: '{point.y}'
                }
            }
			
        },

       

        series: [{
            name: 'שנים',
            colorByPoint: true,
		 useHTML: Highcharts.hasBidiBug,
		 
		
				
            data: $main,
			useHTML:true

        		
        }],
        drilldown: {
            series: $sub,
			useHTML:true,

        }
		
    });
});");
	?>




