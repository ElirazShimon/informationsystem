<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RevenuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'הכנסות';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revenues-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור הכנסה', ['create'], ['class' => 'btn btn-success']) ?>		
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('גרף הכנסות', ['chart2'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],
			
			
			[
				'attribute' => 'date',
				'value' => 'date',
				'format' => 'raw',
				'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date',
						'clientOptions' => [
						'autoclose' => true,
						'format' => 'yyyy-m-dd']
							
						])
			],
			
		
			
          
            'day',
            'cash_desk_784',
            'cash_desk_782',
            'store',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
