<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchedualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'שיבוץ לעובד';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedual-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('שלח שיבוץ', ['create'], ['class' => 'btn btn-info']) ?>
    </p>
	
	<?php if (\Yii::$app->user->can('createUser')) { ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, //
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'month',
            'employeesName',
            'days',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php } ?>
	
	<?php if (!\Yii::$app->user->can('createUser')) { ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, //
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'month',
            'employeesName',
            'days',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php } ?>
	

</div>
