<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Schedual */

$this->title = 'עדכון שיבוץ לחודש ' . $model->month;
$this->params['breadcrumbs'][] = ['label' => 'שיבוץ לעובד', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->month, 'url' => ['view', 'id' => $model->month]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="schedual-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
