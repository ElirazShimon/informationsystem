<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Schedual */

$this->title = $model->month;
$this->params['breadcrumbs'][] = ['label' => 'שיבוץ לעובד', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedual-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עדכן', ['update', 'id' => $model->month], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחק', ['delete', 'id' => $model->month], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'month',
            'employeesName',
            'days',
        ],
    ]) ?>



</div>