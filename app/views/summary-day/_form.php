<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\widgets\TouchSpin;
/* @var $this yii\web\View */
/* @var $model app\models\SummaryDay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-day-form">

    <?php $form = ActiveForm::begin(); ?>

    

	<?= $form->field($model, 'date')->widget(
			DatePicker::className(), [
				// inline too, not bad
				 'inline' => false, 
				 // modify template for custom rendering
				//'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
				'clientOptions' => [
					'autoclose' => true,
					'format' => 'dd/mm/yyyy'
				]
		]);?>

   
	<?=$form->field($model, 'israels')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>

    
	
	<?=$form->field($model, 'tourist')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>

    
	<?=$form->field($model, 'matmon')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>

    <?= $form->field($model, 'events')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
