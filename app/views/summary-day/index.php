<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SummaryDaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'סיכום יום';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-day-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור סיכום יום ', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'date',
				'value' => 'date',
				'format' => 'raw',
				'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date',
						'clientOptions' => [
						'autoclose' => true,
						'format' => 'dd/mm/yyyy']
							
						])
			],
            //'date',
            'israels',
            'tourist',
            'matmon',
            'events',
            // 'notes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
