<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryDay */

$this->title = ' עדכון סיכום ליום: ' . $model->date;
$this->params['breadcrumbs'][] = ['label' => 'סיכום יום', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->date, 'url' => ['view', 'id' => $model->date]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="summary-day-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
