<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryDay */

$this->title = $model->date;
$this->params['breadcrumbs'][] = ['label' => 'סיכום יום', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-day-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עדכון', ['update', 'id' => $model->date], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->date], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'date',
            'israels',
            'tourist',
            'matmon',
            'events',
            'notes',
        ],
    ]) ?>

</div>
