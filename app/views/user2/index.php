<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Employees;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'משתמשים';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>
	

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>
        <?= Html::a('צור משתמש חדש', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
		
		 [
				'attribute' => 'id',
				'label' => 'שם עובד',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->employeessUser->fullname,
				['employees/view', 'id' => $model->employeessUser->id]);
				},
			],
			
			//'id',
		
            'username',
            [
			'attribute' => 'role',
				'label' => 'תפקיד המשתמש',
								
				'value' => function($model){
					return  $model->userole;
				},],
			//'password',
            //'auth_key',
            
           
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
