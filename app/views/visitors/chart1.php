<?php

use miloschuman\highcharts\Highcharts;
?>




<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart1"></div>



<?php	

$sql = "select  SUM(DISTINCT g1.`store`)+ SUM(DISTINCT g2.`store`)+ SUM(DISTINCT g3.`store`) as revo
, SUM(DISTINCT g4.`store`)+ SUM(DISTINCT g5.`store`)+ SUM(DISTINCT g6.`store`)  as revt
, SUM(DISTINCT g7.`store`)+ SUM(DISTINCT g8.`store`)+ SUM(DISTINCT g9.`store`) as revth
, SUM(DISTINCT g10.`store`)+ SUM(DISTINCT g11.`store`)+ SUM(DISTINCT g12.`store`) as revf
from visitors g
left join visitors g1 on g.date = g1.date and month(g.date) = 1 and year(g.date)=2015
left join visitors g2 on g.date = g2.date and month(g.date) = 2 and year(g.date)=2015
left join visitors g3 on g.date = g3.date and month(g.date) = 3 and year(g.date)=2015
left join visitors g4 on g.date = g4.date and month(g.date) = 4 and year(g.date)=2015
left join visitors g5 on g.date = g5.date and month(g.date) = 5 and year(g.date)=2015
left join visitors g6 on g.date = g6.date and month(g.date) = 6 and year(g.date)=2015
left join visitors g7 on g.date = g7.date and month(g.date) = 7 and year(g.date)=2015
left join visitors g8 on g.date = g8.date and month(g.date) = 8 and year(g.date)=2015
left join visitors g9 on g.date = g9.date and month(g.date) = 9 and year(g.date)=2015
left join visitors g10 on g.date = g10.date and month(g.date) = 10 and year(g.date)=2015
left join visitors g11 on g.date = g11.date and month(g.date) = 11 and year(g.date)=2015
left join visitors g12 on g.date = g12.date and month(g.date) = 12 and year(g.date)=2015";

$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	
	'data' => [[$data['revo']*1],[$data['revt']*1],[$data['revth']*1],[$data['revf']*1]]
	];	
}


$main = json_encode($main_data);















/*$sql = "select  SUM(DISTINCT g1.`cash_desk_784`)+ SUM(DISTINCT g2.`cash_desk_784`)+ SUM(DISTINCT g3.`cash_desk_784`) as rev1
, SUM(DISTINCT g4.`cash_desk_784`)+ SUM(DISTINCT g5.`cash_desk_784`)+ SUM(DISTINCT g6.`cash_desk_784`)  as rev2
, SUM(DISTINCT g7.`cash_desk_784`)+ SUM(DISTINCT g8.`cash_desk_784`)+ SUM(DISTINCT g9.`cash_desk_784`) as rev3
, SUM(DISTINCT g10.`cash_desk_784`)+ SUM(DISTINCT g11.`cash_desk_784`)+ SUM(DISTINCT g12.`cash_desk_784`) as rev4
from visitors g
left join visitors g1 on g.date = g1.date and month(g.date) = 1 and year(g.date)=2015
left join visitors g2 on g.date = g2.date and month(g.date) = 2 and year(g.date)=2015
left join visitors g3 on g.date = g3.date and month(g.date) = 3 and year(g.date)=2015
left join visitors g4 on g.date = g4.date and month(g.date) = 4 and year(g.date)=2015
left join visitors g5 on g.date = g5.date and month(g.date) = 5 and year(g.date)=2015
left join visitors g6 on g.date = g6.date and month(g.date) = 6 and year(g.date)=2015
left join visitors g7 on g.date = g7.date and month(g.date) = 7 and year(g.date)=2015
left join visitors g8 on g.date = g8.date and month(g.date) = 8 and year(g.date)=2015
left join visitors g9 on g.date = g9.date and month(g.date) = 9 and year(g.date)=2015
left join visitors g10 on g.date = g10.date and month(g.date) = 10 and year(g.date)=2015
left join visitors g11 on g.date = g11.date and month(g.date) = 11 and year(g.date)=2015
left join visitors g12 on g.date = g12.date and month(g.date) = 12 and year(g.date)=2015";

$rawData = yii::$app->db->createCommand($sql)->queryAll();
$subb_data =[];
foreach ($rawData as $data){
	$subb_data[] =[
	
	'data' => [[$data['rev1']*1],[$data['rev2']*1],[$data['rev3']*1],[$data['rev4']*1]]
	];	
}


$subb = json_encode($subb_data);









$sql = "select  SUM(DISTINCT g1.`cash_desk_782`)+ SUM(DISTINCT g2.`cash_desk_782`)+ SUM(DISTINCT g3.`cash_desk_782`) as rev1
, SUM(DISTINCT g4.`cash_desk_782`)+ SUM(DISTINCT g5.`cash_desk_782`)+ SUM(DISTINCT g6.`cash_desk_782`)  as rev2
, SUM(DISTINCT g7.`cash_desk_782`)+ SUM(DISTINCT g8.`cash_desk_782`)+ SUM(DISTINCT g9.`cash_desk_782`) as rev3
, SUM(DISTINCT g10.`cash_desk_782`)+ SUM(DISTINCT g11.`cash_desk_782`)+ SUM(DISTINCT g12.`cash_desk_782`) as rev4
from visitors g
left join visitors g1 on g.date = g1.date and month(g.date) = 1 and year(g.date)=2015
left join visitors g2 on g.date = g2.date and month(g.date) = 2 and year(g.date)=2015
left join visitors g3 on g.date = g3.date and month(g.date) = 3 and year(g.date)=2015
left join visitors g4 on g.date = g4.date and month(g.date) = 4 and year(g.date)=2015
left join visitors g5 on g.date = g5.date and month(g.date) = 5 and year(g.date)=2015
left join visitors g6 on g.date = g6.date and month(g.date) = 6 and year(g.date)=2015
left join visitors g7 on g.date = g7.date and month(g.date) = 7 and year(g.date)=2015
left join visitors g8 on g.date = g8.date and month(g.date) = 8 and year(g.date)=2015
left join visitors g9 on g.date = g9.date and month(g.date) = 9 and year(g.date)=2015
left join visitors g10 on g.date = g10.date and month(g.date) = 10 and year(g.date)=2015
left join visitors g11 on g.date = g11.date and month(g.date) = 11 and year(g.date)=2015
left join visitors g12 on g.date = g12.date and month(g.date) = 12 and year(g.date)=2015";
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	
	'data' => [[$data['rev1']*1],[$data['rev2']*1],[$data['rev3']*1],[$data['rev4']*1]]	
	];	
}


$main = json_encode($main_data);

*/

?>

<?php		
	$this->registerJs("$(function () {
    $('#chart1').highcharts({		
		
        title: {
            text: 'כמות מבקרים לפי רבעונים לשנת 2015'
        },
        xAxis: {
            categories: ['רבעון 1', 'רבעון 2', 'רבעון 3', 'רבעון 4' ],
			useHTML: true
        },
        labels: {
            items: [{
                html: 'סך הכל כמות מבקרים',
                style: {
                    left: '130px',
                    top: '18px',
					useHTML: true,
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
		tooltip: {
        useHTML: true
    },
	legend: {
            enabled: true,
			useHTML:true
        },
        series: [{
            type: 'column',
            name: 'קופה 784',
            data: [230,450,693,350],
			useHTML: true
        }, {
            type: 'column',
            name: 'קופה 782',
            data: [150,200,362,260],
			useHTML: true
        }, {
            type: 'column',
            name: 'חנות',
            data: [450,300,262,460],
			useHTML: true
        }, {
            type: 'spline',
            name: 'ממוצע',
            data: [276.66, 316.66, 439, 356.66],
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white',
				useHTML: true
            }
        }, {
            type: 'pie',
            name: 'סך המבקרים',
            data: [{
                name: 'חנות',
                y: 1472,
                color: Highcharts.getOptions().colors[0] // Jane's color
            }, {
                name: 'קופה 784',
                y: 1669,
                color: Highcharts.getOptions().colors[1] // John's color
            }, {
                name: 'קופה 782',
                y: 972,
                color: Highcharts.getOptions().colors[2] // Joe's color
            }],
            center: [50, 80],
            size: 100,
            showInLegend: false,
			useHTML: true,
            dataLabels: {
                enabled: false,
				useHTML: true
            }
        }]
    });
});");
?>




