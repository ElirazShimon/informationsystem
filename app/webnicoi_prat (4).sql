-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2016 at 02:01 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webnicoi_prat`
--

-- --------------------------------------------------------

--
-- Table structure for table `armed`
--

CREATE TABLE `armed` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `armed`
--

INSERT INTO `armed` (`id`, `name`) VALUES
(0, 'ללא נשק\r\n'),
(1, 'אקדח רשותי\r\n'),
(2, 'אקדח אישי\r\n'),
(3, 'נשק ארוך ואקדח רשותי\r\n'),
(4, 'נשק ארוך רשותי\r\n'),
(5, 'נשק ארוך אישי\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '255', 1477647124),
('employee', '300972974', 1477644376),
('teamleader', '254', 1477647179);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('createBuroc', 2, 'Admin can create new buroc', NULL, NULL, 1477479905, 1477479905),
('createEmployee', 2, 'Admin can create new employee', NULL, NULL, 1477479905, 1477479905),
('createInvitations', 2, 'Admin can create new invitation', NULL, NULL, 1477479905, 1477479905),
('createProject', 2, 'Team leader can create project', NULL, NULL, 1477479867, 1477479867),
('createRevenues', 2, 'Admin can create new revenue', NULL, NULL, 1477479906, 1477479906),
('createSubscribers', 2, 'Admin can create new subscriber', NULL, NULL, 1477479906, 1477479906),
('createSummary', 2, 'Team leader can create new summary', NULL, NULL, 1477479867, 1477479867),
('createSuppliers', 2, 'Admin can create new supplier', NULL, NULL, 1477479905, 1477479905),
('createUser', 2, 'Admin can create new user', NULL, NULL, 1477479904, 1477479904),
('createVisitors', 2, 'Admin can create new visitor', NULL, NULL, 1477479906, 1477479906),
('deleteBuroc', 2, 'Admin can delete  buroc', NULL, NULL, 1477479905, 1477479905),
('deleteEmployee', 2, 'Admin can delete  employee', NULL, NULL, 1477479905, 1477479905),
('deleteInvitations', 2, 'Admin can delete  invitation', NULL, NULL, 1477479906, 1477479906),
('deleteRevenues', 2, 'Admin can delete  revenue', NULL, NULL, 1477479906, 1477479906),
('deleteSubscribers', 2, 'Admin can delete  subscriber', NULL, NULL, 1477479906, 1477479906),
('deleteSummary', 2, 'Admin can delete  summary', NULL, NULL, 1477479905, 1477479905),
('deleteSuppliers', 2, 'Admin can delete  supplier', NULL, NULL, 1477479905, 1477479905),
('deleteUser', 2, 'Admin can delete users', NULL, NULL, 1477479905, 1477479905),
('deleteVisitors', 2, 'Admin can delete  visitor', NULL, NULL, 1477479906, 1477479906),
('employee', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('indexBuroc', 2, 'Admin can view all burocs', NULL, NULL, 1477479905, 1477479905),
('indexEmployee', 2, 'Index employee', NULL, NULL, 1477479815, 1477479815),
('indexInvitations', 2, 'Admin can view all invitations', NULL, NULL, 1477479905, 1477479905),
('indexProject', 2, 'Index Project', NULL, NULL, 1477479815, 1477479815),
('indexRevenues', 2, 'Admin can view all revenues', NULL, NULL, 1477479906, 1477479906),
('indexSubscribers', 2, 'Admin can view all subscribers', NULL, NULL, 1477479906, 1477479906),
('indexSummary', 2, 'Index summary', NULL, NULL, 1477479815, 1477479815),
('indexSuppliers', 2, 'Admin can view all suppliers', NULL, NULL, 1477479905, 1477479905),
('indexUser', 2, 'Admin can view all users', NULL, NULL, 1477479905, 1477479905),
('indexVisitors', 2, 'Admin can view all visitors', NULL, NULL, 1477479906, 1477479906),
('teamleader', 1, NULL, NULL, NULL, 1477479728, 1477479728),
('updateBuroc', 2, 'Admin can update  buroc', NULL, NULL, 1477479905, 1477479905),
('updateEmployee', 2, 'Admin can update  employee', NULL, NULL, 1477479905, 1477479905),
('updateInvitations', 2, 'Admin can update  invitation', NULL, NULL, 1477479905, 1477479905),
('updateOwnPassword', 2, 'Every user can update his/her own password', NULL, NULL, 1477479815, 1477479815),
('updateOwnProject', 2, 'Team leader can update his only project', NULL, NULL, 1477479867, 1477479867),
('updateOwnSummary', 2, 'Team leader can update his only summary', NULL, NULL, 1477479867, 1477479867),
('updateOwnUser', 2, 'Every user can update his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('updatePassword', 2, 'Admin can update password for all users', NULL, NULL, 1477479905, 1477479905),
('updateProject', 2, 'Team leader can update project', NULL, NULL, 1477479867, 1477479867),
('updateRevenues', 2, 'Admin can update  revenue', NULL, NULL, 1477479906, 1477479906),
('updateSubscribers', 2, 'Admin can update  subscriber', NULL, NULL, 1477479906, 1477479906),
('updateSummary', 2, 'Team leader can update summary', NULL, NULL, 1477479867, 1477479867),
('updateSuppliers', 2, 'Admin can update  supplier', NULL, NULL, 1477479905, 1477479905),
('updateUser', 2, 'Admin can update all users', NULL, NULL, 1477479905, 1477479905),
('updateVisitors', 2, 'Admin can update  visitor', NULL, NULL, 1477479906, 1477479906),
('viewBuroc', 2, 'Admin can view each buroc', NULL, NULL, 1477479905, 1477479905),
('viewEmployee', 2, 'Admin can view each  employee', NULL, NULL, 1477479905, 1477479905),
('viewInvitations', 2, 'Admin can view each invitation', NULL, NULL, 1477479905, 1477479905),
('viewOwnEmployee', 2, 'Every employee can view his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('viewOwnUser', 2, 'Every user can view his/her own profile ', NULL, NULL, 1477479815, 1477479815),
('viewProject', 2, 'View Project', NULL, NULL, 1477479815, 1477479815),
('viewRevenues', 2, 'Admin can view each revenue', NULL, NULL, 1477479906, 1477479906),
('viewSubscribers', 2, 'Admin can view each subscriber', NULL, NULL, 1477479906, 1477479906),
('viewSummary', 2, 'View summary', NULL, NULL, 1477479815, 1477479815),
('viewSuppliers', 2, 'Admin can view each supplier', NULL, NULL, 1477479905, 1477479905),
('viewUser', 2, 'Admin can view each user', NULL, NULL, 1477479905, 1477479905),
('viewVisitors', 2, 'Admin can view each visitor', NULL, NULL, 1477479906, 1477479906);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'createBuroc'),
('admin', 'createEmployee'),
('admin', 'createInvitations'),
('admin', 'createRevenues'),
('admin', 'createSubscribers'),
('admin', 'createSuppliers'),
('admin', 'createUser'),
('admin', 'createVisitors'),
('admin', 'deleteBuroc'),
('admin', 'deleteEmployee'),
('admin', 'deleteInvitations'),
('admin', 'deleteRevenues'),
('admin', 'deleteSubscribers'),
('admin', 'deleteSummary'),
('admin', 'deleteSuppliers'),
('admin', 'deleteUser'),
('admin', 'deleteVisitors'),
('admin', 'indexBuroc'),
('admin', 'indexInvitations'),
('admin', 'indexRevenues'),
('admin', 'indexSubscribers'),
('admin', 'indexSuppliers'),
('admin', 'indexUser'),
('admin', 'indexVisitors'),
('admin', 'teamleader'),
('admin', 'updateBuroc'),
('admin', 'updateEmployee'),
('admin', 'updateInvitations'),
('admin', 'updatePassword'),
('admin', 'updateRevenues'),
('admin', 'updateSubscribers'),
('admin', 'updateSuppliers'),
('admin', 'updateUser'),
('admin', 'updateVisitors'),
('admin', 'viewBuroc'),
('admin', 'viewEmployee'),
('admin', 'viewInvitations'),
('admin', 'viewRevenues'),
('admin', 'viewSubscribers'),
('admin', 'viewSuppliers'),
('admin', 'viewUser'),
('admin', 'viewVisitors'),
('employee', 'indexEmployee'),
('employee', 'indexProject'),
('employee', 'indexSummary'),
('employee', 'updateOwnPassword'),
('employee', 'updateOwnUser'),
('employee', 'viewOwnEmployee'),
('employee', 'viewOwnUser'),
('employee', 'viewProject'),
('employee', 'viewSummary'),
('teamleader', 'createProject'),
('teamleader', 'createSummary'),
('teamleader', 'employee'),
('teamleader', 'updateOwnProject'),
('teamleader', 'updateOwnSummary'),
('teamleader', 'updateProject'),
('teamleader', 'updateSummary');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bstatus`
--

CREATE TABLE `bstatus` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bstatus`
--

INSERT INTO `bstatus` (`id`, `name`) VALUES
(0, 'ממתין להתחלת טיפול'),
(1, 'בטיפול'),
(2, 'ממתין לאישור'),
(3, 'בהשהיה'),
(4, 'טופל');

-- --------------------------------------------------------

--
-- Table structure for table `buroc`
--

CREATE TABLE `buroc` (
  `subject` varchar(100) NOT NULL,
  `treatment` varchar(255) NOT NULL,
  `bstatus` int(100) NOT NULL,
  `DueDate` date NOT NULL,
  `creatDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buroc`
--

INSERT INTO `buroc` (`subject`, `treatment`, `bstatus`, `DueDate`, `creatDate`, `notes`) VALUES
('thfh', 'fghfgh', 0, '2016-10-27', '2016-10-18 20:29:39', 'fghfh'),
('הוצאת עובדים לקורס פקחים', 'להתקשר לאמיר ולירון', 0, '2016-10-11', '2016-10-09 09:52:31', 'חשוב  מאוד'),
('חפיפת עובד ללימודי סגירת חודש', 'לקבוע עם חנניה זמנים שהוא והעובד יכולים', 1, '2016-10-26', '2016-10-09 10:15:05', 'לוודא עם חנניה שזה קורה בדיקת עדכון');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `armed` int(11) NOT NULL,
  `Percent_of_jobs` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `cellphone`, `adress`, `role`, `armed`, `Percent_of_jobs`, `image`, `email`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'יאיר', 'יאיר', '52', 'fh', 0, 0, 0, 'uploads/יאיר.png', 'vhgvfh@dgf.com', 1477647540, 1477649883, 255, 255),
(9, 'bmb', 'g ', '52', 'fh', 0, 0, 0, 'uploads/bmb.png', '', 1477904900, 1477904900, 255, 255),
(66, 'נכע', 'העכ', '0542', 'gnxg', 0, 0, 0, 'uploads/נכע.pdf', 'vhgunhvfhh@dgf.com', 0, 0, 0, 0),
(225, ' jyju ', 'ju ', '054836987', 'hf', 5, 2, 3, 'uploads/ jyju .pdf', 'vhgvfh@dgf.com', 0, 0, 0, 0),
(254, 'kbj', 'ju ', '52', 'fh', 0, 0, 0, '', 'vhgvfh@dgf.com', 1476956551, 1476956551, 203254646, 203254646),
(255, 'jv', 'juj', '52', 'fh', 0, 0, 0, 'uploads/jv.pdf', 'vhgunvfhh@dgf.com', 0, 0, 0, 0),
(553, ' jyju ', 'n,n', '52', 'fh', 0, 0, 0, '', 'vhgunvfh@dgf.com', 1476956871, 1476956871, 203254646, 203254646),
(987, 'טליה', 'טליה', '52', 'gnxg', 0, 0, 0, 'uploads/טליה.png', 'vhgvfh@dgf.com', 1477648367, 1477648367, 255, 255),
(5245, 'o;lo', 'o;o', '05', 'jyjy', 0, 0, 0, '', 'vhgunvfhh@dgf.com', 1476956520, 1476956520, 203254646, 203254646),
(20325, 'h', 'hhgh', '52543', 'jkhu', 0, 0, 0, '', 'nvv@gnbmbj.com', 0, 0, 0, 0),
(25254, 'bmb', 'g ', '0542', 'gnxg', 0, 0, 0, 'uploads/bmb.png', 'vhgvfh@dgf.com', 1476958152, 1476958152, 203254646, 203254646),
(25255, 'gbf', 'g ', '0542', 'gnxg', 0, 0, 0, '', 'vhgunvfh@dgf.com', 1476957452, 1476957452, 203254646, 203254646),
(54535, 'gbf', 'ju ', '0542', 'gnxg', 0, 0, 0, '', 'vhgunvfhh@dgf.com', 1476957497, 1476957497, 203254646, 203254646),
(545345, 'bmb', 'g ', '0542', 'fh', 0, 0, 0, 'uploads/bmb.png', 'vhgvfh@dgf.com', 1476958333, 1476958333, 203254646, 203254646),
(1516615, 'kghjg', 'cgfhf', '15615313', 'xddfd', 5, 5, 2, '', '', 0, 0, 0, 0),
(111222233, 'jfshkufh', 'sdfygsydfgs', '2224455', 'drggtg', 0, 0, 0, '', '', 0, 0, 0, 0),
(203254644, 'bmb', 'n,n', '546832', '4546', 0, 0, 0, '', '', 0, 0, 0, 0),
(300972973, 'ליעד', 'ניזרי', '546220232', 'גבריהו חיים 7/1', 0, 0, 0, '', '', 0, 0, 0, 0),
(300972974, 'אלירז', 'שמעון', '52', 'fh', 0, 0, 0, 'uploads/אלירז.png', 'vhgunvfhh@dgf.com', 1477647441, 1477647441, 255, 255);

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `item_name` varchar(200) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `open_date` date NOT NULL,
  `due_date` date NOT NULL,
  `quantity_order` int(200) NOT NULL,
  `approval_status` enum('approved','not approved') NOT NULL,
  `order_status` enum('provided','not provided') NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_name` varchar(200) NOT NULL,
  `quantity_in_stock` int(100) NOT NULL,
  `price_each` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_name`, `quantity_in_stock`, `price_each`) VALUES
('מלגזה', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1476869722),
('m140506_102106_rbac_init', 1476986097);

-- --------------------------------------------------------

--
-- Table structure for table `percent_of_jobs`
--

CREATE TABLE `percent_of_jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percent_of_jobs`
--

INSERT INTO `percent_of_jobs` (`id`, `name`) VALUES
(0, 'עד 40%'),
(2, 'עד 70%'),
(3, '100%');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) NOT NULL,
  `define_project` varchar(255) NOT NULL,
  `team_leader` varchar(255) NOT NULL,
  `employee` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `define_project`, `team_leader`, `employee`, `location`, `due_date`, `notes`) VALUES
(1, 'nhc', 'nn', '1', 'vfv', '12-Oct-2016', 'b'),
(2, 'vbvcbvc', '254', '111222233', 'vhvh', '18-Oct-2016', 'vvhbv');

-- --------------------------------------------------------

--
-- Table structure for table `revenues`
--

CREATE TABLE `revenues` (
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `revenues`
--

INSERT INTO `revenues` (`date`, `day`, `cash_desk_784`, `cash_desk_782`, `store`) VALUES
('2012-01-01', 'א', 7854, 0, 0),
('2012-02-01', 'ד', 6374, 0, 0),
('2012-03-01', 'ה', 9632, 0, 0),
('2012-04-01', 'א', 1478, 0, 0),
('2012-05-01', 'ג', 6524, 0, 0),
('2012-06-01', 'ו', 6341, 0, 0),
('2012-07-01', 'א', 5412, 0, 0),
('2012-08-01', 'ד', 6524, 0, 0),
('2012-09-01', 'ש', 4712, 0, 0),
('2012-10-01', 'ב', 1256, 0, 0),
('2012-11-01', 'ה', 3654, 0, 0),
('2012-12-01', 'ש', 3624, 0, 0),
('2013-01-01', 'ג', 6547, 0, 0),
('2013-02-01', 'ה', 2541, 0, 0),
('2013-03-01', 'ו', 4785, 0, 0),
('2013-04-01', 'ב', 2589, 0, 0),
('2013-05-01', 'ד', 6985, 0, 0),
('2013-06-01', 'ש', 8745, 0, 0),
('2013-07-01', 'ב', 9685, 0, 0),
('2013-08-01', 'ד', 4785, 0, 0),
('2013-09-01', 'א', 3652, 0, 0),
('2013-10-01', 'ג', 6352, 0, 0),
('2013-11-01', 'ו', 8521, 0, 0),
('2013-12-01', 'א', 4185, 0, 0),
('2014-01-01', 'ד', 6354, 0, 0),
('2014-02-01', 'ש', 4178, 0, 0),
('2014-03-01', 'ש', 6389, 0, 0),
('2014-04-01', 'ג', 6387, 0, 0),
('2014-05-01', 'ה', 9638, 0, 0),
('2014-06-01', 'א', 6385, 0, 0),
('2014-07-01', 'ג', 9638, 0, 0),
('2014-08-01', 'ג', 1423, 0, 0),
('2014-09-01', 'ב', 3654, 0, 0),
('2014-10-01', 'ד', 6352, 0, 0),
('2014-11-01', 'ש', 4521, 0, 0),
('2014-12-01', 'ב', 5241, 0, 0),
('2015-01-01', 'ה', 797, 0, 32),
('2015-01-02', '6', 607, 0, 70),
('2015-01-03', 'ש', 1949, 0, 177),
('2015-01-04', 'א', 58, 0, 0),
('2015-01-05', 'ב', 146, 0, 0),
('2015-01-06', 'ג', 98, 0, 310),
('2015-01-07', 'ד', 116, 0, 26),
('2015-01-08', 'ה', 261, 0, 0),
('2015-02-01', 'א', 7854, 0, 254),
('2015-03-01', 'א', 6589, 0, 0),
('2015-04-01', 'ד', 5412, 0, 0),
('2015-05-01', 'ו', 6352, 0, 0),
('2015-06-01', 'ב', 3214, 0, 0),
('2015-07-01', 'ד', 4152, 0, 0),
('2015-08-01', 'ש', 7485, 0, 0),
('2015-09-01', 'ג', 7486, 0, 0),
('2015-10-01', 'ה', 9658, 0, 0),
('2015-11-01', 'א', 5412, 0, 0),
('2015-12-01', 'ג', 6352, 0, 0),
('2016-01-01', 'ו', 2587, 0, 0),
('2016-02-01', 'ב', 6385, 74, 0),
('2016-03-01', 'ג', 9874, 0, 0),
('2016-04-01', 'ו', 1236, 0, 0),
('2016-05-01', 'א', 5632, 0, 0),
('2016-06-01', 'ד', 6374, 0, 0),
('2016-07-01', 'ו', 6541, 0, 0),
('2016-08-01', 'ב', 6398, 0, 0),
('2016-09-01', 'ה', 8569, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(0, 'ראש צוות - פקח - נשק'),
(1, 'ראש צוות - פקח'),
(2, 'ראש צוות'),
(3, 'פקח'),
(4, 'עובד - קופאי'),
(5, 'עובד'),
(6, 'שומר לילה');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `summaryday`
--

CREATE TABLE `summaryday` (
  `date` date NOT NULL,
  `israels` int(255) DEFAULT NULL,
  `tourist` int(255) DEFAULT NULL,
  `matmon` int(255) DEFAULT NULL,
  `events` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `summaryday`
--

INSERT INTO `summaryday` (`date`, `israels`, `tourist`, `matmon`, `events`, `notes`) VALUES
('2016-10-03', 131, 5, 455, '', ''),
('2016-10-08', 720, 65, 9, 'ללא', 'hkkj\r\nhjkhjk\r\n\r\n\r\nghkhk\r\n\r\n\r\n\r\nhkfh'),
('2016-10-09', 33, 55, 0, '', 'חיגממכ\r\nחבידייכ\r\nחגימב'),
('2016-10-10', 33, 46, 99, '', ''),
('2016-10-13', 52, 63, 54, '', ''),
('2016-10-29', 52, 63, 54, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_name` varchar(100) NOT NULL,
  `phoneNum` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `contactPhone` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_name`, `phoneNum`, `address`, `contact`, `contactPhone`, `category`, `notes`) VALUES
('ביובית "פעיל" שירותי ביוב', '26565777', 'רמה 19 , גבעת זאב', 'דרור', '505305757', 'פינוי ביוב', ''),
('עמנואל שלם', '200000000', '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(0, 'ff', '$2y$13$22unOZhSodytfHs4bGd7heG.YuWRpvzbgsbgDDIBYsQNpTUtVaMgS', 'EauMfRzdSbTGlyiCHUjJHaJuOjtUQFKp', 1477905157, 1477905157, 255, 255),
(254, 'teamleader', '$2y$13$3Pvg9yLywuxbKfqcXTKecOgmaIcRGHle66xQm782bXZnxzknGa5S6', 'WnVA1xkwvdC5AT069FcKCwDE2r9UPlQa', 1477560874, 1477647177, NULL, 255),
(255, 'admin', '$2y$13$CSBZcZvsV0f4DATidh4Cvuw8t2a8W02/RsvXB/LoyOASkrstoxxjS', '7O_FUKnntTMBo-jXxks_H9rCcYr9Ta5j', 1477570055, 1477647122, NULL, 255),
(300972974, 'employee', '$2y$13$wMfE3u3n0mcx8Xtnrn3VHedM5/Qhi0k2AEH5y/CoOUFXeL6xKTMUy', 'pehIu5RJlsjjI2alP4HdCPNZX08TB4pU', 1477644374, 1477644374, 255, 255);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `cash_desk_784` int(255) NOT NULL,
  `cash_desk_782` int(255) NOT NULL,
  `store` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `armed`
--
ALTER TABLE `armed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `bstatus`
--
ALTER TABLE `bstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buroc`
--
ALTER TABLE `buroc`
  ADD PRIMARY KEY (`subject`),
  ADD KEY `status` (`bstatus`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `armed` (`armed`),
  ADD KEY `Percent_of_jobs` (`Percent_of_jobs`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`item_name`,`supplier_name`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `percent_of_jobs`
--
ALTER TABLE `percent_of_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revenues`
--
ALTER TABLE `revenues`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `summaryday`
--
ALTER TABLE `summaryday`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `buroc`
--
ALTER TABLE `buroc`
  ADD CONSTRAINT `bstatus` FOREIGN KEY (`bstatus`) REFERENCES `bstatus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `Percent_of_jobs` FOREIGN KEY (`Percent_of_jobs`) REFERENCES `percent_of_jobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `armed` FOREIGN KEY (`armed`) REFERENCES `armed` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
